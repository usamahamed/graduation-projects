

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Mahmoud
 */
public class Server_operations {
    //Connection parametrs

    Connection conn;
    Statement stat;
    String Query;
    ResultSet rs;
    //Names of the candidates
    String[] Workers;
    String[] Farmers;
    String[] Categories;
    String[] republic;
    //Images of the candidates
    Blob[] WorkersImages;
    Blob[] FarmersImages;
    Blob[] CategoriesImages;
    Blob[] republicImages;
    //Citizen info
    static int ID;
    static String City;
    String Council;

     Database db;
   static int current_vote = 0;

    public Server_operations(int Citizen_ID) {
        ID=Citizen_ID;
        Set_current_Vote(2);
        int y=get_current_Vote();


        try {
              db = new Database();
            db.connect();
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Driver loaded");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/voting", "root", "root");
            stat = conn.createStatement();        } catch (Exception ex) {
            Logger.getLogger(Server_operations.class.getName()).log(Level.SEVERE, null, ex);
        }

       if(y==2){
           Council="People";
           SelectFarmersCandidates();
            SelectCategoriesCandidates();
            SelectWorkersCandidates();
       }
       else if(y==3){
          Council= "Shura";
          SelectFarmersCandidates();
            SelectCategoriesCandidates();
            SelectWorkersCandidates();
       }if(y==1){
           selectrepublic();
       }

    }

    public boolean SelectCitizenCity() {
        try {
            Query = "select ID from citizens where ID = " + ID;
            rs = stat.executeQuery(Query);
            if (rs.next()) {
                Query = "select City from citizens where ID = " + ID;
                rs = stat.executeQuery(Query);
                rs.next();
                City = rs.getString(1);
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "User doesn't exist");
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public void SelectFarmersCandidates() {
        if (SelectCitizenCity()) {
            try {
                Query = "select Name,Symbol from candidates where City = '" + City + "' and Council = '" + Council + "' and Category = 'Farmer'";
                rs = stat.executeQuery(Query);
                int i = 0;
                while (rs.next()) {
                    i++;
                }
                if (i == 0) {
                    System.out.println("Ezaaaaaaaaaaaaaaaaaaaaaaaaaaaaay");
                } else {
                    Farmers = new String[i];
                    FarmersImages = new Blob[i];
                    rs = stat.executeQuery(Query);
                    for (int j = 0; j < i; j++) {
                        rs.next();
                        Farmers[j] = rs.getString(1);
                        FarmersImages[j] = rs.getBlob(2);
                        System.out.println("the name: " + rs.getString(1) + "   Symbol: " + rs.getBlob(2));
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(Server_operations.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void SelectWorkersCandidates() {
        if (SelectCitizenCity()) {
            try {
                Query = "select Name,Symbol from candidates where City = '" + City + "' and Council = '" + Council + "' and Category = 'Worker'";
                rs = stat.executeQuery(Query);
                int i = 0;
                while (rs.next()) {
                    i++;
                }
                if (i == 0) {
                    System.out.println("Ezaaaaaaaaaaaaaaaaaaaaaaaaaaaaay");
                } else {
                    Workers = new String[i];
                    WorkersImages = new Blob[i];
                    rs = stat.executeQuery(Query);
                    for (int j = 0; j < i; j++) {
                        rs.next();
                        Workers[j] = rs.getString(1);
                        WorkersImages[j] = rs.getBlob(2);
                        System.out.println("the name: " + rs.getString(1) + "  Symbol: " + rs.getBlob(2));
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(Server_operations.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void SelectCategoriesCandidates() {
        if (SelectCitizenCity()) {
            try {
                Query = "select Name,Symbol from candidates where City = '" + City + "' and Council = '" + Council + "' and Category = 'Categories'";
                rs = stat.executeQuery(Query);
                int i = 0;
                while (rs.next()) {
                    i++;
                }
                if (i == 0) {
                    System.out.println("Ezaaaaaaaaaaaaaaaaaaaaaaaaaaaaay");
                } else {
                    Categories = new String[i];
                    CategoriesImages = new Blob[i];
                    rs = stat.executeQuery(Query);
                    for (int j = 0; j < i; j++) {
                        rs.next();
                        Categories[j] = rs.getString(1);
                        CategoriesImages[j] = rs.getBlob(2);
                        System.out.println("the name: " + rs.getString(1) + "  Symbol: " + rs.getBlob(2));
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(Server_operations.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void main(String[] args) {
        Server_operations o = new Server_operations(3);
        o.SelectFarmersCandidates();
        o.SelectWorkersCandidates();
        o.SelectCategoriesCandidates();
    }
     public String Check(double id, String sid) {

        String State = "no";
        System.out.println("id=  " + id + "\n SID =" + sid);
        try {


            String str = "SELECT ID,password FROM citizens ";
            ResultSet rs6 = db.search(str);
            while (rs6.next()) {
                if (rs6.getDouble(1) == id && rs6.getString(2).equals(sid)) {
                    State = "yes";
                    System.out.println("user found");
                }
            }

            String str1 = "SELECT * FROM login ";
            ResultSet rs1 = db.search(str1);
            while (rs1.next()) {
                if (rs1.getInt(1) == id) {
                    State = "no";
                    System.out.println(State);
                    System.out.println("user online");
                }
            }

            String str2 = "SELECT * FROM voted ";
            ResultSet rs2 = db.search(str2);
            while (rs2.next()) {
                if (rs2.getInt(1) == id) {
                    State = "no";
                    System.out.println("user voted before");
                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return State;
    }
//end Method

    public static int get_current_Vote() {
        return current_vote;
    }

    public  static void Set_current_Vote(int ii) {
        Server_operations.current_vote = ii;
    }

    public void GOOnLine(int User_ID) {
        try {
            db.DML("insert  into login values(" + User_ID + ")");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void GOffLine(int User_ID) {
        try {
            db.DML("delete from login where " + User_ID );
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void Updatevoted(int User_ID) {
        try {
            db.DML("insert " + User_ID + " into voted");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void updateresult(String name) {
        try {
            ResultSet rs1 = db.search("select council_result.Number_Votes,candidates.ID from council_result,candidates where council_result.ID = candidates.ID and candidates.name='" + name.trim() + "'");
            rs1.next();
            int res = rs1.getInt(1);
            int id = rs1.getInt(2);
            System.out.println("sssssssssssssssssssssssss+"+res+"uuuuuuuuuuuuuu"+id);
            res++;
            db.DML("UPDATE council_result SET `Number_Votes`=" + res + " WHERE ID =" + id);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            ex.printStackTrace();
        }
    }

    private void selectrepublic() {
        try {
                Query = "select Name,Symbol from republic";
                rs = db.search(Query);
                int i = 0;
                while (rs.next()) {
                    i++;
                }
                if (i == 0) {
                    System.out.println("Ezaaaaaaaaaaaaaaaaaaaaaaaaaaaaay");
                } else {
                    republic = new String[i];
                    republicImages = new Blob[i];
                    rs = stat.executeQuery(Query);
                    for (int j = 0; j < i; j++) {
                        rs.next();
                        republic[j] = rs.getString(1);
                        republicImages[j] = rs.getBlob(2);
                        System.out.println("the name: " + rs.getString(1) + "   Symbol: " + rs.getBlob(2));
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(Server_operations.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    public void updateresult_republic(String name) {
        try {
            ResultSet rs1 = db.search("select republic_result.Number_Votes,republic.ID from republic_result,republic where republic_result.ID = republic.ID and republic.Name='" + name + "'");
            rs1.next();
            int id = rs1.getInt(2);
            int res = rs1.getInt(1);
            res++;
           int x= db.DML("UPDATE republic_result SET `Number_Votes`=" + res + " WHERE ID =" + id);
           if(x==1)
                System.out.println("result update success");
        } catch (SQLException ex) {
           // JOptionPane.showMessageDialog(null, ex.getMessage());
            ex.printStackTrace();
        }
    }

}
