

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.security.*;
import javax.crypto.*;
import javax.crypto.spec.*;

/**
 *
 * @author Mahmoud
 */
public class Security {

    String Key = "1234567891234567";
    int PlainTextLength;

    public byte[] EncryptString(String Text) {
        try {
            PlainTextLength = Text.length();
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            Key key = new SecretKeySpec(Key.getBytes(), 0, Key.getBytes().length, "AES");
            int blocksize = 16;
            int ciphertextLength = 0;
            int remainder = Text.getBytes().length % blocksize;
            if (remainder == 0) {
                ciphertextLength = Text.getBytes().length;
            } else {
                ciphertextLength = Text.getBytes().length - remainder + blocksize;
            }
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] ciphertext = new byte[ciphertextLength];
            cipher.doFinal(Text.getBytes(), 0, Text.getBytes().length, ciphertext, 0);
            return ciphertext;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public String DecryptStringBytes(byte[] CipherArray) {
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            Key key = new SecretKeySpec(Key.getBytes(), 0, Key.getBytes().length, "AES");
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] decrypted = new byte[PlainTextLength];
            cipher.doFinal(CipherArray, 0, CipherArray.length, decrypted, 0);
            String PlainText = new String(decrypted);
            return PlainText;

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public byte[] EncryptBytes(byte[] BytesToEncrypt) {
        try {
            PlainTextLength = BytesToEncrypt.length;
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            Key key = new SecretKeySpec(Key.getBytes(), 0, Key.getBytes().length, "AES");
            int blocksize = 16;
            int ciphertextLength = 0;
            int remainder = BytesToEncrypt.length % blocksize;
            if (remainder == 0) {
                ciphertextLength = BytesToEncrypt.length;
            } else {
                ciphertextLength = BytesToEncrypt.length - remainder + blocksize;
            }
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] ciphertext = new byte[ciphertextLength];
            cipher.doFinal(BytesToEncrypt, 0, BytesToEncrypt.length, ciphertext, 0);
            return ciphertext;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public byte[] DecryptBytes(byte[] BytesToDecrypt) {
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            Key key = new SecretKeySpec(Key.getBytes(), 0, Key.getBytes().length, "AES");
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] decrypted = new byte[PlainTextLength];
            cipher.doFinal(BytesToDecrypt, 0, BytesToDecrypt.length, decrypted, 0);
            return decrypted;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void mode(String s) {
        char[] Ch;
        String Text = null;
        int j;
        int FinalSypher;
        Ch = s.toCharArray();
        for (int i = 0; i < 7; i++) {
            j = Ch[i];
            while (j > 9) {
                j = j / 9;
            }
            Text = Text + j;
        }
        FinalSypher = Integer.parseInt(Text);
        System.out.println("" + FinalSypher);
    }
}
