/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ResultTable.java
 *
 * Created on Jul 10, 2010, 3:15:29 AM
 */


import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

/**
 *
 * @author Mahmoud
 */
public class ResultTable extends javax.swing.JPanel {

    Connection conn;
    Statement stat;
    ResultSet rs;
    Statement stat1;
    ResultSet rs1;
    Statement stat2;
    ResultSet rs2;
    String Query;
    int First_Farmer_ID;
    int First_Farmer_Result;
    int First_Worker_ID;
    int First_Worker_Result;
    int First_Categories_ID;
    int First_Categories_Result;
    int i = 0;

    /** Creates new form ResultTable */
    public ResultTable() {
        try {
            initComponents();
            Class.forName("com.mysql.jdbc.Driver");

            conn = DriverManager.getConnection("jdbc:mysql://localhost/voting", "root", "root");



            Query = "select * from governs";
            stat2 = conn.createStatement();
            rs2 = stat2.executeQuery(Query);
            while (rs2.next()) {

                Query = "select ID,Name,Govern,City,party from Candidates where Category = 'Farmer' and Govern = '" + rs2.getString(1) + "' and City = '" + rs2.getString(2) + "'";
                stat = conn.createStatement();
                rs = stat.executeQuery(Query);
                stat1 = conn.createStatement();
                while (rs.next()) {
                    Query = "select Number_Votes from council_result where ID = " + rs.getInt(1);
                    rs1 = stat1.executeQuery(Query);
                    rs1.next();
                    if (rs1.getInt(1) > First_Farmer_Result) {
                        First_Farmer_Result = rs1.getInt(1);
                        First_Farmer_ID = rs.getInt(1);
                    }
                }
                //First Worker
                Query = "select ID from Candidates where Category = 'Worker' and Govern = '" + rs2.getString(1) + "' and City = '" + rs2.getString(2) + "'";
                rs = stat.executeQuery(Query);
                stat1 = conn.createStatement();
                while (rs.next()) {
                    Query = "select Number_Votes from council_result where ID = " + rs.getInt(1);
                    rs1 = stat1.executeQuery(Query);
                    rs1.next();
                    if (rs1.getInt(1) > First_Worker_Result) {
                        First_Worker_Result = rs1.getInt(1);
                        First_Worker_ID = rs.getInt(1);
                    }
                }
                //First Categories
                Query = "select ID from Candidates where Category = 'Categories' and Govern = '" + rs2.getString(1) + "' and City = '" + rs2.getString(2) + "'";
                rs = stat.executeQuery(Query);
                stat1 = conn.createStatement();
                while (rs.next()) {
                    Query = "select Number_Votes from council_result where ID = " + rs.getInt(1);
                    rs1 = stat1.executeQuery(Query);
                    rs1.next();
                    if (rs1.getInt(1) > First_Categories_Result) {
                        First_Categories_Result = rs1.getInt(1);
                        First_Categories_ID = rs.getInt(1);
                    }
                }
                if (First_Farmer_Result < First_Categories_Result) {
                    if (First_Farmer_Result < First_Worker_Result) {
                        Query = "select Name,Govern,City,party from Candidates where ID = " + First_Categories_ID;
                        stat = conn.createStatement();
                        rs = stat.executeQuery(Query);
                        rs.next();
                        jTable1.setValueAt(rs.getString(1), i, 4);
                        jTable1.setValueAt(rs.getString(2), i, 0);
                        jTable1.setValueAt(rs.getString(3), i, 1);
                        jTable1.setValueAt(rs.getString(4), i, 3);
                        jTable1.setValueAt("Categories", i, 2);
                        jTable1.setValueAt(First_Categories_Result, i, 5);

                        i++;

                        Query = "select Name,Govern,City,party from Candidates where ID = " + First_Worker_ID;
                        stat = conn.createStatement();
                        rs = stat.executeQuery(Query);
                        rs.next();
                        jTable1.setValueAt(rs.getString(1), i, 4);
                        jTable1.setValueAt(rs.getString(2), i, 0);
                        jTable1.setValueAt(rs.getString(3), i, 1);
                        jTable1.setValueAt(rs.getString(4), i, 3);
                        jTable1.setValueAt("Worker", i, 2);
                        jTable1.setValueAt(First_Worker_Result, i, 5);
                    } else {
                        Query = "select Name,Govern,City,party from Candidates where ID = " + First_Categories_ID;
                        stat = conn.createStatement();
                        rs = stat.executeQuery(Query);
                        rs.next();
                        jTable1.setValueAt(rs.getString(1), i, 4);
                        jTable1.setValueAt(rs.getString(2), i, 0);
                        jTable1.setValueAt(rs.getString(3), i, 1);
                        jTable1.setValueAt(rs.getString(4), i, 3);
                        jTable1.setValueAt("Categories", i, 2);
                        jTable1.setValueAt(First_Categories_Result, i, 5);

                        i++;

                        Query = "select Name,Govern,City,party from Candidates where ID = " + First_Farmer_ID;
                        stat = conn.createStatement();
                        rs = stat.executeQuery(Query);
                        rs.next();
                        jTable1.setValueAt(rs.getString(1), i, 4);
                        jTable1.setValueAt(rs.getString(2), i, 0);
                        jTable1.setValueAt(rs.getString(3), i, 1);
                        jTable1.setValueAt(rs.getString(4), i, 3);
                        jTable1.setValueAt("Farmer", i, 2);
                        jTable1.setValueAt(First_Farmer_Result, i, 5);
                    }
                } else {
                    if (First_Categories_Result < First_Worker_Result) {
                        Query = "select Name,Govern,City,party from Candidates where ID = " + First_Farmer_ID;
                        stat = conn.createStatement();
                        rs = stat.executeQuery(Query);
                        rs.next();
                        jTable1.setValueAt(rs.getString(1), i, 4);
                        jTable1.setValueAt(rs.getString(2), i, 0);
                        jTable1.setValueAt(rs.getString(3), i, 1);
                        jTable1.setValueAt(rs.getString(4), i, 3);
                        jTable1.setValueAt("Farmer", i, 2);
                        jTable1.setValueAt(First_Farmer_Result, i, 5);

                        i++;

                        Query = "select Name,Govern,City,party from Candidates where ID = " + First_Worker_ID;
                        stat = conn.createStatement();
                        rs = stat.executeQuery(Query);
                        rs.next();
                        jTable1.setValueAt(rs.getString(1), i, 4);
                        jTable1.setValueAt(rs.getString(2), i, 0);
                        jTable1.setValueAt(rs.getString(3), i, 1);
                        jTable1.setValueAt(rs.getString(4), i, 3);
                        jTable1.setValueAt("Worker", i, 2);
                        jTable1.setValueAt(First_Worker_Result, i, 5);
                    } else {
                        Query = "select Name,Govern,City,party from Candidates where ID = " + First_Categories_ID;
                        stat = conn.createStatement();
                        rs = stat.executeQuery(Query);
                        rs.next();
                        jTable1.setValueAt(rs.getString(1), i, 4);
                        jTable1.setValueAt(rs.getString(2), i, 0);
                        jTable1.setValueAt(rs.getString(3), i, 1);
                        jTable1.setValueAt(rs.getString(4), i, 3);
                        jTable1.setValueAt("Categories", i, 2);
                        jTable1.setValueAt(First_Categories_Result, i, 5);

                        i++;

                        Query = "select Name,Govern,City,party from Candidates where ID = " + First_Farmer_ID;
                        stat = conn.createStatement();
                        rs = stat.executeQuery(Query);
                        rs.next();
                        jTable1.setValueAt(rs.getString(1), i, 4);
                        jTable1.setValueAt(rs.getString(2), i, 0);
                        jTable1.setValueAt(rs.getString(3), i, 1);
                        jTable1.setValueAt(rs.getString(4), i, 3);
                        jTable1.setValueAt("Farmer", i, 2);
                        jTable1.setValueAt(First_Farmer_Result, i, 5);
                    }
                }
                First_Farmer_Result = 0;
                First_Categories_Result = 0;
                First_Worker_Result = 0;
                i++;
            }
        } catch (Exception ex) {
            Logger.getLogger(ResultTable.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Govern", "City", "Category", "Party", "Candidate", "Number of votes"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 594, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 268, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables

    public static void main(String[] args) {
        JFrame x = new JFrame();
        x.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        x.pack();
        ResultTable r = new ResultTable();
        x.setSize(700, 400);
        x.setLocationRelativeTo(null);
        x.setVisible(true);
        x.add(r);
    }

}
