

import java.io.IOException;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.util.Rotation;


public class PieChart extends JPanel {
    ChartPanel chartPanel;
	public PieChart(String chartTitle ,String[] ChoisesNames ,int[] ChoisesValues){
        // This will create the dataset
        PieDataset dataset = createDataset(ChoisesNames,ChoisesValues);

        // based on the dataset we create the chart
        JFreeChart chart = createChart(dataset, chartTitle);

        // we put the chart into a panel
         chartPanel = new ChartPanel(chart);

        // default size
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));

        // add it to our application
        add(chartPanel);
    }
    /**
     * Creates a sample dataset
     */
    private  PieDataset createDataset(String[] ChoisesNames ,int[] ChoisesValues){
        DefaultPieDataset result = new DefaultPieDataset();
        for(int i = 0;i < ChoisesValues.length ;i++){
            result.setValue(ChoisesNames[i],ChoisesValues[i]);
        }
        return result;
    }
    /**
     * Creates a chart
     */
    private JFreeChart createChart(PieDataset dataset, String title) {
        JFreeChart chart = ChartFactory.createPieChart3D(title,dataset,true,true,false);
        PiePlot3D plot = (PiePlot3D) chart.getPlot();
        plot.setStartAngle(290);
        plot.setDirection(Rotation.CLOCKWISE);
        plot.setForegroundAlpha(0.5f);
        return chart;
    }
    public void save() throws IOException{
     chartPanel.doSaveAs();
    }
    
}