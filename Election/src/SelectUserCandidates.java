/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Mahmoud
 */
public class SelectUserCandidates {
    //Connection parametrs

    Connection conn;
    Statement stat;
    String Query;
    ResultSet rs;
    //Names of the candidates
    String[] Workers;
    String[] Farmers;
    String[] Categories;
    //Images of the candidates
    Blob[] WorkersImages;
    Blob[] FarmersImages;
    Blob[] CategoriesImages;
    //Citizen info
    static int ID;
    static String City;
    String Council;

    public SelectUserCandidates(int Citizen_ID, String Council) {
        ID = Citizen_ID;
        this.Council = Council;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Driver loaded");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/voting", "root", "root");
            stat = conn.createStatement();
        } catch (Exception ex) {
            Logger.getLogger(SelectUserCandidates.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean SelectCitizenCity() {
        try {
            Query = "select ID from citizens where ID = " + ID;
            rs = stat.executeQuery(Query);
            if (rs.next()) {
                Query = "select City from citizens where ID = " + ID;
                rs = stat.executeQuery(Query);
                rs.next();
                City = rs.getString(1);
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "User doesn't exist");
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public void SelectFarmersCandidates() {
        if (SelectCitizenCity()) {
            try {
                Query = "select Name,Symbol from candidates where City = '" + City + "' and Council = '" + Council + "' and Category = 'Farmer'";
                rs = stat.executeQuery(Query);
                int i = 0;
                while (rs.next()) {
                    i++;
                }
                if (i == 0) {
                    System.out.println("Ezaaaaaaaaaaaaaaaaaaaaaaaaaaaaay");
                } else {
                    Farmers = new String[i];
                    FarmersImages = new Blob[i];
                    rs = stat.executeQuery(Query);
                    for (int j = 0; j < i; j++) {
                        rs.next();
                        Farmers[j] = rs.getString(1);
                        FarmersImages[j] = rs.getBlob(2);
                        System.out.println("the name: " + rs.getString(1) + "   Symbol: " + rs.getBlob(2));
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(SelectUserCandidates.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void SelectWorkersCandidates() {
        if (SelectCitizenCity()) {
            try {
                Query = "select Name,Symbol from candidates where City = '" + City + "' and Council = '" + Council + "' and Category = 'Worker'";
                rs = stat.executeQuery(Query);
                int i = 0;
                while (rs.next()) {
                    i++;
                }
                if (i == 0) {
                    System.out.println("Ezaaaaaaaaaaaaaaaaaaaaaaaaaaaaay");
                } else {
                    Workers = new String[i];
                    WorkersImages = new Blob[i];
                    rs = stat.executeQuery(Query);
                    for (int j = 0; j < i; j++) {
                        rs.next();
                        Workers[j] = rs.getString(1);
                        WorkersImages[j] = rs.getBlob(2);
                        System.out.println("the name: " + rs.getString(1) + "  Symbol: " + rs.getBlob(2));
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(SelectUserCandidates.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void SelectCategoriesCandidates() {
        if (SelectCitizenCity()) {
            try {
                Query = "select Name,Symbol from candidates where City = '" + City + "' and Council = '" + Council + "' and Category = 'Categories'";
                rs = stat.executeQuery(Query);
                int i = 0;
                while (rs.next()) {
                    i++;
                }
                if (i == 0) {
                    System.out.println("Ezaaaaaaaaaaaaaaaaaaaaaaaaaaaaay");
                } else {
                    Categories = new String[i];
                    CategoriesImages = new Blob[i];
                    rs = stat.executeQuery(Query);
                    for (int j = 0; j < i; j++) {
                        rs.next();
                        Categories[j] = rs.getString(1);
                        CategoriesImages[j] = rs.getBlob(2);
                        System.out.println("the name: " + rs.getString(1) + "  Symbol: " + rs.getBlob(2));
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(SelectUserCandidates.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void main(String[] args) {
        SelectUserCandidates o = new SelectUserCandidates(3, "Shura");
        o.SelectFarmersCandidates();
        o.SelectWorkersCandidates();
        o.SelectCategoriesCandidates();
    }
}
