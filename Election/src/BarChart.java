

import java.io.IOException;
import javax.swing.JPanel;
import org.jfree.chart.*;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;


public class BarChart extends JPanel {
     ChartPanel chartPanel;
	public BarChart(String chartTitle ,String[] ChoisesNames ,int[] ChoisesValues){
        // This will create the dataset
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (int i=0;i<ChoisesValues.length;i++){
            dataset.setValue( ChoisesValues[i], ChoisesNames[i], ChoisesNames[i]);
        }
        // based on the dataset we create the chart
        JFreeChart chart = ChartFactory.createBarChart3D(chartTitle,"", "", dataset, PlotOrientation.VERTICAL, true,true, false);
        // we put the chart into a panel
        chartPanel = new ChartPanel(chart);
        // default size
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        // add it to our application
        add(chartPanel);
    }
  public void save() throws IOException{
     chartPanel.doSaveAs();
    
    }
}