//import javax.crypto.KeyGenerator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.security.Key;
import java.security.InvalidKeyException;
import javax.crypto.spec.SecretKeySpec;

public class LocalEncrypter {
    char[] Ch;
    String Text = "";
    int j;
    int FinalSypher;
    byte[] encryptionBytes;
    private static String algorithm = "DES";
    private static byte[] secretKey = {(byte) 0x2b, (byte) 0x7e, (byte) 0x15, (byte) 0x16,(byte) 0x28, (byte) 0xae, (byte) 0xd2,(byte) 0xa6};
    private static String secretKeyAlgorithm = "DES";
    private static Key key = null;
    private static Cipher cipher = null;
    public LocalEncrypter(String s) {
        try {
//        key = KeyGenerator.getInstance(algorithm).generateKey();
            key = new SecretKeySpec(secretKey,0,secretKey.length,secretKeyAlgorithm);
            cipher = Cipher.getInstance(algorithm);
            encryptionBytes = encrypt(s);
            mode(new String(encryptionBytes));
        } catch (Exception ex) {
            Logger.getLogger(LocalEncrypter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private byte[] encrypt(String input)throws InvalidKeyException,BadPaddingException,IllegalBlockSizeException {
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] inputBytes = input.getBytes();
        return cipher.doFinal(inputBytes);
    }
//    private String decrypt(byte[] encryptionBytes)throws InvalidKeyException,BadPaddingException,IllegalBlockSizeException {
//        cipher.init(Cipher.DECRYPT_MODE, key);
//        byte[] recoveredBytes =cipher.doFinal(encryptionBytes);
//        String recovered = new String(recoveredBytes);
//        return recovered;
//    }
    public void mode(String s){
        Ch = s.toCharArray();
        for(int i = 0;i < 7;i++){
            j = Ch[i];
            while(j > 9){
                j = j/9;
            }
            Text = Text + j;
        }
        FinalSypher = Integer.parseInt(Text);
        System.out.println(""+FinalSypher);
    }
    public static void main(String[] args) {
        LocalEncrypter l = new LocalEncrypter("12345678");
    }
}