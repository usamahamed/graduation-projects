import com.sun.lwuit.Command;
import com.sun.lwuit.Component;
import com.sun.lwuit.Dialog;
import com.sun.lwuit.Display;
import com.sun.lwuit.Font;
import com.sun.lwuit.Form;
import com.sun.lwuit.Image;
import com.sun.lwuit.Label;
import com.sun.lwuit.List;
import com.sun.lwuit.animations.Transition;
import com.sun.lwuit.events.ActionEvent;
import com.sun.lwuit.events.ActionListener;
import com.sun.lwuit.geom.Dimension;
import com.sun.lwuit.list.ListCellRenderer;
import com.sun.lwuit.plaf.Border;
import com.sun.lwuit.plaf.Style;
import com.sun.lwuit.plaf.UIManager;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.microedition.io.StreamConnection;
import javax.microedition.midlet.MIDlet;


 class Small_selection extends List implements ActionListener
{

public static StreamConnection connection1;
public List Material;
Command select;
    Image[] letters = new Image[2];
String[] descriptions = {"Show Description ", "Vote on Topic "};

    //form
   public static Form selectionForm;
	//the list
	private List alphaList;
        //duration of transition in milliseconds
	private int duration = 750;

	//number of steps in transition
	private int numOfSteps = 10;

  Command sel=  new Command("Select");

	//images for creating list Content1s
	//public Image[] letters = new Image[3];

	//texts for creating list Content1s
	//private String[] descriptions = {"Download Material", "Ask Question", "Take Quiz"};

	//array holding Content1s of list
	private Content1[] Content1s = new Content1[2];

        StreamConnection con;
        DataInputStream in;
        DataOutputStream out;

        MIDlet mid;
        public Small_selection(MIDlet mid,StreamConnection conn,DataInputStream inp,DataOutputStream outp)
        {
            this.con=conn;
            this.in=inp;
            this.out=outp;
             this.mid=mid;
        }


	public void startApp()
	{

       		//initialize the LWUIT Display
		//and register this MIDlet
       		Display.init(this);

		//create and set a style for scrollbar
		Style scrollStyle = new Style();
		scrollStyle.setFgColor(0x5555ff);
		scrollStyle.setBgColor(0xdddd99);
		UIManager.getInstance().setComponentStyle("Scroll", scrollStyle);

		//create and set a style for scrollthumb
		Style scrollThumbStyle = new Style();
		scrollThumbStyle.setMargin(Component.LEFT, 1);
		scrollThumbStyle.setMargin(Component.RIGHT, 1);
		UIManager.getInstance().setComponentStyle("ScrollThumb", scrollThumbStyle);

		//create a new form
		 selectionForm = new Form("Select Choise");
//    Transition out = new BlindsTransition(duration, numOfSteps, Display.getInstance().getDisplayWidth()/10);
  //          selectionForm.setTransitionOutAnimator(out);

		//no scrollbar for the form
		selectionForm.setScrollable(false);

		//get width of the form
		int width = selectionForm.getWidth();

		//create a font
		Font font = Font.createSystemFont(Font.FACE_PROPORTIONAL,Font.STYLE_BOLD,Font.SIZE_LARGE);

		//set text color for title
		selectionForm.getTitleStyle().setFgColor(0x99cc00);

		//set font style for title
		selectionForm.getTitleStyle().setFont(font);

		//set background color for the title bar
		selectionForm.getTitleStyle().setBgColor(0x555555);

		//create a new style object
		Style menuStyle = new Style();

		//set the background color -- the same as for title bar
		menuStyle.setBgColor(0x555555);

		//set the text color for soft button
		menuStyle.setFgColor(0x99cc00);

		//set font style for soft button
		menuStyle.setFont(font);

		//now install the style for soft button
		selectionForm.setSoftButtonStyle(menuStyle);

		//set a background color for the form
		selectionForm.getStyle().setBgColor(0);

		//create and add 'Exit' command to the form
		//the command id is 0


        selectionForm.addCommand(sel);

		//this MIDlet is the listener for the form's command
		selectionForm.setCommandListener(this);

		try
		{
			letters[0] = Image.createImage("/a.png");
			letters[1] = Image.createImage("/b.png");


		}
		catch(java.io.IOException ioe)
		{
                    ioe.printStackTrace();
		}

		//create a list using the images and texts
		alphaList = getList(letters, descriptions);

		//get an AlphaListRenderer instance and install
		AlphaListRenderer renderer = new AlphaListRenderer();
		alphaList.setListCellRenderer(renderer);

		//hint for alphaList size
		alphaList.setPreferredSize(new Dimension(width-2, alphaList.getPreferredH()));


		//add a listener to sense key/pointer action
		alphaList.addActionListener(new ActionListener()
			{
public void actionPerformed(ActionEvent ae)
{
//showDialog();
 Command c = ae.getCommand();
}
});
//fixes the position of the selected item
		//alphaList.setFixedSelection(List.FIXED_CENTER);
		//alphaList.setFixedSelection(List.FIXED_NONE_ONE_ELEMENT_MARGIN_FROM_EDGE);

		//add alphaList to the form
		selectionForm.addComponent(alphaList);

		//show the form
       		selectionForm.show();
   	}

   	public void pauseApp()
	{
   	}

   	public void destroyApp(boolean unconditional)
	{
   	}

	//act on the command
	public void actionPerformed(ActionEvent ae)
	{
		Command cmd = ae.getCommand();
 if(cmd==sel){
 int x=alphaList.getSelectedIndex();
 System.out.println("xxxxxxxxx     :  "+x);
if(x==0)  /// show description
{
try {
                    out.writeInt(500);
                    out.flush();
                    out.flush();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
new Description().startApp();
}
 if(x==1)   // go to vote

 {
                try {
                    out.writeInt(700);
                    out.flush();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
new Vote().startApp();


 }
  }
         else if(cmd.getCommandName().equals("Back")){
            try {
//
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            }

	}

	//returns a list created from an image array and a text array
	private List getList(Image[] images, String[] texts)
	{
int length = images.length;


		//the list will have 24 elements
		for(int i = 0; i < 2; i++)
		{
			//repetitively use 6 images and strings
			int index = i % length;

			//Content1 is the object used as list element
			//create and load 24 Content1 objects
			Content1s[i] = new Content1(images[index],texts[index]);
		}

		return new List(Content1s);
	}

	private void showDialog()
	{
		//create the dialog
		Dialog d = new Dialog("Selection");

		//give it a border
		d.getContentPane().getStyle().setBorder(Border.createBevelRaised());

		//font for the label
		Font f = Font.createSystemFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM);

		//create label for heading
		Label hLabel = new Label("Your selection:");

		hLabel.getStyle().setFont(Font.createSystemFont(Font.FACE_SYSTEM,Font.STYLE_BOLD,Font.SIZE_LARGE));
		hLabel.getStyle().setBgTransparency(0);
		hLabel.getStyle().setFgColor(0);
		hLabel.getStyle().setMargin(Label.BOTTOM, 15);

		//add the heading to dialog
		d.addComponent(hLabel);

		//get selected item
		Content1 selected = (Content1)alphaList.getSelectedItem();

		//create label for showing selected item
		Label sLabel = new Label(selected.getText());

		sLabel.setIcon(selected.getIcon());

		//set preferred dimension and style
		//sLabel.setPreferredSize(new Dimension(d.getWidth(), sLabel.getPreferredH()));
		sLabel.getStyle().setFont(f);
		sLabel.getStyle().setBgColor(0x0000ff);
		sLabel.getStyle().setFgColor(0xffffff);
		sLabel.getStyle().setBorder(Border.createRoundBorder(15, 10, 0));

		//add the label to the form
		d.addComponent(sLabel);

		//set bg color for dialog
		d.getContentPane().getStyle().setBgColor(0xff8040);

		//set style attributes for the dialog titlebar
		d.getTitleStyle().setBgColor(0xa43500);
		d.getTitleStyle().setFgColor(0xffffff);
		d.getTitleStyle().setFont(Font.createSystemFont(Font.FACE_SYSTEM,Font.STYLE_BOLD,Font.SIZE_MEDIUM));
		d.getTitleStyle().setBorder(Border.createBevelRaised());

		//create and set style for dialog menubar
		Style s = new Style();
		s.setBgColor(0xa43500);
		s.setFgColor(0xffffff);
		s.setFont(Font.createSystemFont(Font.FACE_SYSTEM,Font.STYLE_BOLD,Font.SIZE_MEDIUM));
		s.setBorder(Border.createBevelRaised());
		d.setSoftButtonStyle(s);

		//add two commands to dialog
		d.addCommand(new Command("OK"));
		//d.addCommand(new Command("Back"));

		//show the dialog
		d.showDialog();
	}
}
class Content1
{
	private Image letter;
	private String text;

	public Content1(Image i, String s)
	{
		letter = i;
		text = s;
	}

	public Image getIcon()
	{
		return letter;
	}

	public String getText()
	{
		return text;
	}
}

class AlphaListRenderer extends Label implements ListCellRenderer
{
	//create new AlphaListRenderer
	public AlphaListRenderer()
	{
		super();
	}

	public Component getListCellRendererComponent(List list, Object value, int index, boolean isSelected)
	{
		//cast the value object into a Content1
		Content1 entry = (Content1)value;

		//get the icon of the Content1 and set it for this label
		setIcon(entry.getIcon());

		//get the text of the Content1 and set it for this label
		setText(entry.getText());

		//set transparency
		getStyle().setBgTransparency((byte)128);

		//set background and foreground colors
		//depending on whether the item is selected or not
		if(isSelected)
		{
			getStyle().setBgColor(0x0000ff);
			getStyle().setFgColor(0xffffff);
		}
		else
		{
			getStyle().setBgColor(0xff0000);
		}

		return this;
	}

	//initialize for drawing focus
	public Component getListFocusComponent(List list)
	{
		setText("");
        	setIcon(null);
		getStyle().setBgColor(0x0000ff);
        	getStyle().setBgTransparency(80);
        	return this;
        }

	public void repaint()
	{
	}
}

