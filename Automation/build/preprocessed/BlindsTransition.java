import com.sun.lwuit.Component;
import com.sun.lwuit.Dialog;
import com.sun.lwuit.Graphics;
import com.sun.lwuit.Image;
import com.sun.lwuit.animations.Transition;

public class BlindsTransition extends Transition
{
    	private StepMotion motion;
    	private int step;
    	private int duration;
    	private int blindWidth;
	private int numOfStripes;
	private int stripeWidth = 24;
	private Image iBuffer;
	private boolean firstCycle;

	public BlindsTransition(int duration, int step, int stripeWidth)
    	{
		//duration of transition
		this.duration = duration;

		//number of steps for implementing transition
        	this.step = step;

		//width of each strip
		this.stripeWidth = stripeWidth;
    	}

    	public void initTransition()
    	{
		//initialize variables
		//required to set up motion

		//the outgoing form/dialog/component
        	Component source = getSource();

		//the incoming form/dialog/component
        	Component destination = getDestination();

		firstCycle = true;
        	blindWidth = 0;
        	int w = source.getWidth();
        	int h = source.getHeight();
        	int startOffset = 0;
		numOfStripes = w/stripeWidth;

		//add extra stripe to cover entire width if required
		//this may be a partial stripe
		if(w%stripeWidth != 0)
		{
			numOfStripes++;
		}

		//initialize buffer
		if (iBuffer == null)
		{
            		iBuffer = Image.createImage(w, h);
        	}
		
		//create a StepMotion object for this transition
  		motion = new StepMotion(startOffset, stripeWidth, duration, step);

                //when a dialog is involved the tinted source
                //is painted once only to be reused as required
		if(getDestination() instanceof Dialog)
		{
			doPaint(iBuffer.getGraphics(), getSource(), 0, 0, 0, 0);
		}

		//start the movement for the transition
        	motion.start();
    	}

    	public boolean animate()
	{
		//see if there is a motion object
		//and check if the transition is completed
        	if(motion == null || motion.isFinished())
		{
			//in either case terminate transition
            		return false;
        	}

		//if transition is to continue
		//get the new position
        	blindWidth = motion.getStep();
		
		//continue with transition
        	return true;
    	}
    
    	public void paint(Graphics g)
    	{
		//return if new position is not available
		if(blindWidth == -1)
		{
        		return;
		}

		//otherwise paint
		paintBlinds(g);
    	}

	//support for grabage clean up
    	public void cleanup()
	{
		super.cleanup();
		iBuffer = null;
    	}

	private void paintBlinds(Graphics g)
	{
        	Component source = getSource();
        
        	// if this is the first form being displayed we
		//can't do a transition since we have no source form
        	if (source == null)
		{ 
            		return;           
        	}

        	Component dest = getDestination();

		// dialog animation is different
		//so paint tinted or untinted form as required
		if(source instanceof Dialog)
		{
			if(firstCycle)
			{
				//draw destination and the dialog once

				//comment out following statement
				//to get a different effect
				doPaint(g, dest, 0, 0, 0, 0);

				//paint the dialog
				doPaint(g, source, 0, 0, 0, 0);

				firstCycle = false;
			}

			//render transition over the dialog
			for(int i = 0; i < numOfStripes; i++)
			{
				g.setClip(source.getAbsoluteX(), source.getAbsoluteY(), source.getWidth(), source.getHeight());
				g.clipRect((source.getAbsoluteX()+i*stripeWidth), source.getAbsoluteY(), blindWidth, source.getHeight());
        			doPaint(g, dest, 0, 0, 0, 0);
			}

			return;
		} 

		if(dest instanceof Dialog)
		{
			if(firstCycle)
			{
				//draw tinted form once
				g.drawImage(iBuffer, 0, 0);
				firstCycle = false;
			}

			//render transition over the tinted source
			for(int i = 0; i < numOfStripes; i++)
			{
				int crx = dest.getAbsoluteX()+i*stripeWidth;
				int crw = blindWidth;

				g.setClip(dest.getAbsoluteX(), dest.getAbsoluteY(), dest.getWidth(), dest.getHeight());
				g.clipRect(crx, dest.getAbsoluteY(), blindWidth, dest.getHeight());

        			doPaint(g, dest, 0, 0, crx, crw);
			}

			return;
		}

		//not dialog
		for(int i = 0; i < numOfStripes; i++)
		{
			//initialize and set clip
			g.setClip(dest.getAbsoluteX(), dest.getAbsoluteY(), source.getWidth(), source.getHeight());
			g.clipRect((dest.getAbsoluteX()+i*stripeWidth), dest.getAbsoluteY(), blindWidth, source.getHeight());

        		doPaint(g, dest, 0, 0, 0, 0);
		}
        
    	}

    	private void doPaint(Graphics g, Component cmp, int x, int y, int crx, int crw)
    	{
		//get original clip details
        	int cx = g.getClipX();
        	int cy = g.getClipY();
        	int cw = g.getClipWidth();
        	int ch = g.getClipHeight();

		if(cmp instanceof Dialog)
		{
			Dialog dialog = (Dialog)cmp;
			dialog.getContentPane().paintComponent(g, false);

			//menu bar will exist only if there is at least one command
			if(dialog.getCommandCount() > 0)
			{
				Component menuBar = dialog.getSoftButton(0).getParent();
				g.setClip(0, 0, cmp.getWidth(), cmp.getHeight());

				if(getDestination() instanceof Dialog)
				{
					g.clipRect(crx, cy, crw, ch);
				}

				menuBar.paintComponent(g, false);
			}

			if(getDestination() instanceof Dialog)
			{
				g.setClip(0, 0, cmp.getWidth(), cmp.getHeight());
				g.clipRect(crx, cy, crw, ch);
			}

			dialog.getTitleComponent().paintComponent(g, false);

			//restore clip
			g.setClip(cx, cy, cw, ch);
			return;
		}

		//make sure cooridates are properly aligned
         	g.translate(x, y);

		//get it painted
        	cmp.paintComponent(g, false);

		//restore original values
         	g.translate(-x, -y);
        	g.setClip(cx, cy, cw, ch);
    	}
    
	//return a functionally equivalent transition object
	public Transition copy()
	{
		return new BlindsTransition(duration, step, stripeWidth);
	}
}
