
import com.sun.lwuit.ButtonGroup;
import com.sun.lwuit.Command;
import com.sun.lwuit.Component;
import com.sun.lwuit.Container;
import com.sun.lwuit.Dialog;
import com.sun.lwuit.Display;
import com.sun.lwuit.Font;
import com.sun.lwuit.Form;
import com.sun.lwuit.Image;
import com.sun.lwuit.Label;
import com.sun.lwuit.List;
import com.sun.lwuit.RadioButton;
import com.sun.lwuit.TextArea;
import com.sun.lwuit.animations.Transition;
import com.sun.lwuit.events.ActionEvent;
import com.sun.lwuit.events.ActionListener;
import com.sun.lwuit.geom.Dimension;
import com.sun.lwuit.layouts.GridLayout;
import com.sun.lwuit.list.DefaultListCellRenderer;
import com.sun.lwuit.list.DefaultListModel;
import com.sun.lwuit.list.ListCellRenderer;
import com.sun.lwuit.plaf.Border;
import com.sun.lwuit.plaf.Style;
import com.sun.lwuit.plaf.UIManager;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.String;
import java.util.Timer;
import java.util.TimerTask;
import javax.microedition.io.Connector;
import javax.microedition.io.SocketConnection;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.file.FileConnection;

class SELCTION_List extends List implements ActionListener {

    public static StreamConnection connection1;
    public List Material;
    Command select;
    Image[] letters = new Image[3];
    String[] descriptions = {"Download Material", "Ask Question", "Take Quiz"};
    //form
    public static Form selectionForm;
    //the list
    private List alphaList;
    //duration of transition in milliseconds
    private int duration = 750;
    //number of steps in transition
    private int numOfSteps = 10;
    static int x;
    int Counter = 0;
    Command sel = new Command("Select");
    //images for creating list Content1s
    //public Image[] letters = new Image[3];
    //texts for creating list Content1s
    //private String[] descriptions = {"Download Material", "Ask Question", "Take Quiz"};
    //array holding Content1s of list
    private Content1[] Content1s = new Content1[3];
    private Content1[] Content1_Subj = new Content1[5];
    ////my variable                                                             =======================    ======= @@@@
    Content1 connn;
    Form endQ;
    Form demoForm;
    List subjects;
    Image images[];
    String names[];
    // quize
    Form farr[];
    Command anse;
    String quiz[];
    List li;
    int cuurent_form = 0;
    TextArea[] QQQ;
    RadioButton[] mealPrefs;
    ButtonGroup bg[];
    int Answer[];
    // string subject to take it exame and quize number
    String Subject_TakeQ;
    String Quize_Nm;
    Command bak;
    Form Quizes;
    Form notav;
    Command Back_subj;
    Command ok;
    Command down;
    Command quizeNotAv, FinalC;
  String names1[] = null;
    public void startApp() {

        //initialize the LWUIT Display
        //and register this MIDlet
        Display.init(this);

        //create and set a style for scrollbar
        Style scrollStyle = new Style();
        scrollStyle.setFgColor(0x5555ff);
        scrollStyle.setBgColor(0xdddd99);
        UIManager.getInstance().setComponentStyle("Scroll", scrollStyle);

        //create and set a style for scrollthumb
        Style scrollThumbStyle = new Style();
        scrollThumbStyle.setMargin(Component.LEFT, 1);
        scrollThumbStyle.setMargin(Component.RIGHT, 1);
        UIManager.getInstance().setComponentStyle("ScrollThumb", scrollThumbStyle);

        //create a new form
        selectionForm = new Form("Select Choise");
        Transition out = new BlindsTransition(duration, numOfSteps, Display.getInstance().getDisplayWidth() / 10);
        selectionForm.setTransitionOutAnimator(out);

        //no scrollbar for the form
        selectionForm.setScrollable(false);

        //get width of the form
        int width = selectionForm.getWidth();

        //create a font
        Font font = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_LARGE);

        //set text color for title
        selectionForm.getTitleStyle().setFgColor(0x99cc00);

        //set font style for title
        selectionForm.getTitleStyle().setFont(font);

        //set background color for the title bar
        selectionForm.getTitleStyle().setBgColor(0x555555);

        //create a new style object
        Style menuStyle = new Style();

        //set the background color -- the same as for title bar
        menuStyle.setBgColor(0x555555);

        //set the text color for soft button
        menuStyle.setFgColor(0x99cc00);

        //set font style for soft button
        menuStyle.setFont(font);

        //now install the style for soft button
        selectionForm.setSoftButtonStyle(menuStyle);

        //set a background color for the form
        selectionForm.getStyle().setBgColor(0);

        //create and add 'Exit' command to the form
        //the command id is 0


        selectionForm.addCommand(sel);

        //this MIDlet is the listener for the form's command
        selectionForm.setCommandListener(this);

        try {
            letters[0] = Image.createImage("/a.png");
            letters[1] = Image.createImage("/b.png");
            letters[2] = Image.createImage("/c.png");

        } catch (java.io.IOException ioe) {
        }

        //create a list using the images and texts
        alphaList = getList(letters, descriptions);

        //get an AlphaListRenderer instance and install
        AlphaListRenderer renderer = new AlphaListRenderer();
        alphaList.setListCellRenderer(renderer);

        //hint for alphaList size
        alphaList.setPreferredSize(new Dimension(width - 2, alphaList.getPreferredH()));


        //add a listener to sense key/pointer action
        alphaList.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                //showDialog();
                Command c = ae.getCommand();

            }
        });

        //fixes the position of the selected item
        //alphaList.setFixedSelection(List.FIXED_CENTER);
        //alphaList.setFixedSelection(List.FIXED_NONE_ONE_ELEMENT_MARGIN_FROM_EDGE);

        //add alphaList to the form
        selectionForm.addComponent(alphaList);

        //show the form
        selectionForm.show();
    }

    public void pauseApp() {
    }

    public void destroyApp(boolean unconditional) {
    }

    //act on the command
    public void actionPerformed(ActionEvent ae) {
        Command cmd = ae.getCommand();


        if (cmd == FinalC) {
            selectionForm.show();
        }


        if (cmd == quizeNotAv) {
            demoForm.show();
        }

        if (cmd == Back_subj) {          /// display subject agian to choose avilable quize in another subject

            System.out.println(" in bak ya naaaaaaaaaaaaaaaaaaaaaassssssssssss");

            demoForm.show();

            try {
                First.conn.out.writeUTF("back");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            Reciev_registered_subject();

        }


        if (cmd == sel) {
            x = alphaList.getSelectedIndex();
            System.out.println("in class selection list :  " + x);
            if (x == 0) {
                try {

                    First.conn.out.writeInt(300);
                    First.conn.out.flush();

                    Reciev_registered_subject();
                     
                    ShowForm();
                    
                    demoForm.removeCommand(ok);
                    down = new Command("go to");
                    demoForm.addCommand(down);
                    
                    /*********Recive Material***********************/
                    demoForm.setCommandListener(new ActionListener() {

                        public void actionPerformed(ActionEvent ae) {
                            Command cmd = ae.getCommand();
                            if (cmd.getCommandName().equals("go to")) {
                                try {

                                    First.conn.out.writeUTF(names[subjects.getSelectedIndex()]);
                                    First.conn.out.flush();
                                   Reciev_avalible_peresen();
                                   // reciveMaterial();
                                    System.out.println("after write");
                                } catch (IOException ex) {
                                    ex.printStackTrace();
                                }

                        }
 else if (cmd.getCommandName().equals("back")) {
     First.mainForm.show();
                            }
                        }
                    });

                  

                        System.out.println ("write successful");
//                    re();  ///pdf
                    } catch  (IOException ex) {
                    ex.printStackTrace();
                }
            }
            if (x == 1) {
                try {

                    First.conn.out.writeInt(200);
                    First.conn.out.flush();
                    System.out.println("write successful200");
                    ASK_QUES();

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            if (x == 2) // take Quize                                                      =================================$$$$$$=====
            {



                try {
                    First.conn.out.writeInt(100);
                    First.conn.out.flush();
                    System.out.println(" After writing 100 ");
                    Reciev_registered_subject();
                } catch (Exception m) {
                }
                ShowForm();
                EndQuize end = new EndQuize();

                Timer t = new Timer();
                t.schedule(end, 75000);
            }
        }//End if
        else if (cmd.getCommandName().equals("Bck")) {
            First.mainForm.show();
        } else if (cmd.getCommandName().equals("OK")) {
            int f = subjects.getSelectedIndex();
            System.out.println(" the selected subjecxt to take exame is " + names[f]);
            try {


                Subject_TakeQ = names[f];
                First.conn.out.writeUTF(names[f]);
                First.conn.out.flush();
                ReciveQuizes();

            } catch (IOException ex) {
                ex.printStackTrace();
            }

        } else if (cmd.getCommandName().equals("Quize")) {
            try {
                int g = li.getSelectedIndex();   // avialable quizes

                Quize_Nm = quiz[g];

                First.conn.out.writeUTF(Quize_Nm);       /// quize number
                First.conn.out.flush();
                First.conn.out.writeUTF(Subject_TakeQ);      /// subject to take exame
                First.conn.out.flush();

            } catch (IOException ex) {
                ex.printStackTrace();
            }


            RecievQuesions();
        } // display next question
        else if (cmd.getCommandName().equals("Next")) {

            System.out.println(" the selected index in radio button is " + bg[cuurent_form].getSelectedIndex());


            if (cuurent_form == farr.length - 1) {
                Answer[cuurent_form] = bg[cuurent_form].getSelectedIndex();
                for (int i = 0; i < Answer.length; i++) {
                    System.out.println(" answers is " + Answer[i]);
                }

                End_Quize_Send_Answer();


            } else {
                Answer[cuurent_form] = bg[cuurent_form].getSelectedIndex() ;

                cuurent_form++;
                farr[cuurent_form].setTitle("Question is " + cuurent_form);
                farr[cuurent_form].show();


            }

        }

        if (cmd == bak) /// bake in avialble subject that want to take exam
        {

            try {
                selectionForm.show();
                First.conn.out.writeUTF("back");
                First.conn.out.flush();
            } catch (Exception m) {
            }

        }


    }

    //////////////////////////////////////////////////                ======================================
    public void Reciev_registered_subject() {
        try {
            // read number of subject
            int no = First.conn.in.readInt();
            System.out.println("After reading no ");
            names = new String[no];
            images = new Image[no];
            for (int i = 0; i < no; i++) {
                names[i] = First.conn.in.readUTF();
                System.out.println(" names " + names[i]);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
  public void Reciev_avalible_peresen() {
      
      try {
            // read number of subject
            int no = First.conn.in.readInt();
            System.out.println("After reading no "+no);
            names1= new String[no];
            for (int i = 0; i < no; i++) {
                names1[i] = First.conn.in.readUTF();
                System.out.println(" names " + names1[i]);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        /*************************************/
            Form demoForm = new Form("Peresentation");

		//create a font
		Font font = Font.createSystemFont(Font.FACE_PROPORTIONAL,Font.STYLE_BOLD,Font.SIZE_LARGE);

		//set text color for title
		demoForm.getTitleStyle().setFgColor(0x99cc00);

		//set font style for title
		demoForm.getTitleStyle().setFont(font);

		//set background color for the title bar
		demoForm.getTitleStyle().setBgColor(0x555555);

		//create a new style object
		Style menuStyle = new Style();

		//set the background color -- the same as for title bar
		menuStyle.setBgColor(0x555555);

		//set the text color for soft button
		menuStyle.setFgColor(0x99cc00);

		//set font style for soft button
		menuStyle.setFont(font);

		//now install the style for soft button
		demoForm.setSoftButtonStyle(menuStyle);

		//set a background color for the form
		demoForm.getStyle().setBgColor(0x656974);

		//create and add 'Exit' command to the form
		//the command id is 0
		demoForm.addCommand(new Command("Back", 0));
                demoForm.addCommand(new Command("Download", 1));


		//this MIDlet is the listener for the form's command
	


		//String[] items = { "Red", "Blue", "Green", "Yellow" };

		// Initialize a default list model with “item”
		DefaultListModel simpleListModel = new DefaultListModel(names1);

		// Create a List with “simpleListModel”
		final List simpleList = new List(simpleListModel);

		//create a ListCellRenderer and install
		DefaultListCellRenderer dlcr = new DefaultListCellRenderer();
		simpleList.setListCellRenderer(dlcr);

		//set prototype to ensure adequate width
		simpleList.setRenderingPrototype("WWWW");

		//set transparency for list
		simpleList.getStyle().setBgTransparency(64);
                 Style lStyle = new Style();
		lStyle.setFgColor(0);
		lStyle.setFgSelectionColor(0xffffff);
		lStyle.setBgSelectionColor(0x0000ff);
		lStyle.setFont(Font.createSystemFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_LARGE));
		dlcr.setStyle(lStyle);

		demoForm.addComponent(simpleList);
	demoForm.setCommandListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
               Command cmd=ae.getCommand();
               if(cmd.getCommandName().equals("Download")){
                    try {
                        First.conn.out.writeUTF(names1[simpleList.getSelectedIndex()]);
                        First.conn.out.flush();
                          reciveMaterial();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
               }
            }
        });
		//show the form
       		demoForm.show();
        /******************************/
    }

    public void reciveMaterial() {
        System.out.println("Inside Recive Material");
        try {
            StreamConnection sc = (StreamConnection) Connector.open("socket://169.254.78.219:5000", Connector.READ_WRITE);
            InputStream inputstream = sc.openInputStream();
            String str;
            ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
            int ch;
            while ((ch = inputstream.read()) != -1) {
                bytestream.write(ch);

            }
            
            str = new String(bytestream.toByteArray());
            bytestream.close();
            inputstream.close();
            writeTextFile("file:///E:/mathcopy.pdf", str);
            System.out.println("Sucessful Save material");
            Form f = new Form("Confirmation Recive");

		//create a font
		Font font = Font.createSystemFont(Font.FACE_PROPORTIONAL,Font.STYLE_BOLD,Font.SIZE_LARGE);

		//set text color for title
		f.getTitleStyle().setFgColor(0x99cc00);

		//set font style for title
		f.getTitleStyle().setFont(font);

		//set background color for the title bar
		f.getTitleStyle().setBgColor(0x555555);

		//create a new style object
		Style menuStyle = new Style();

		//set the background color -- the same as for title bar
		menuStyle.setBgColor(0x555555);

		//set the text color for soft button
		menuStyle.setFgColor(0x99cc00);

		//set font style for soft button
		menuStyle.setFont(font);

		//now install the style for soft button
		f.setSoftButtonStyle(menuStyle);

		//set a background color for the form
		f.getStyle().setBgColor(0x656974);

		//create and add 'Exit' command to the form
		//the command id is 0
                f.addComponent(new Label("You Sucess to download"));
		f.addComponent(new Label(names[subjects.getSelectedIndex()]));
		f.addCommand(new Command("Back", 0));

		//this MIDlet is the listener for the form's command
		f.setCommandListener(new ActionListener() {

                public void actionPerformed(ActionEvent ae) {
                   Command cmd=ae.getCommand();
                   if(cmd.getCommandName().equals("Back")){
                       
                            //First.conn.out.writeUTF("ba");
                           
                        try {
                             selectionForm.show();
                            First.conn.out.writeUTF("10");
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                       
                   }
                }
            });

		//show the form
       		f.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void writeTextFile(String fName, String text) {
        DataOutputStream os = null;
        FileConnection fconn = null;
        try {
            fconn = (FileConnection) Connector.open(fName, Connector.READ_WRITE);
            if (!fconn.exists()) {
                fconn.create();
//                Alert al=new Alert("file not found ");
//                display.setCurrent(al);
            }
            if (!fconn.exists()) {
                fconn.mkdir();
                System.out.println("osama2");
            }
            os = fconn.openDataOutputStream();
            os.write(text.getBytes());
//            Alert al=new Alert("after writing to file ");
//            display.setCurrent(al);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (null != os) {
                    os.close();
                }
                if (null != fconn) {
                    fconn.close();
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /// this is method to recive  Question
    public void RecievQuesions() {
        try {
            int NoQuestion = First.conn.in.readInt();

            farr = new Form[NoQuestion];  // this is no of from

            Answer = new int[NoQuestion];
            QQQ = new TextArea[NoQuestion];

            bg = new ButtonGroup[NoQuestion];

            anse = new Command("Next");
            for (int h = 0; h < NoQuestion; h++) {
                farr[h] = new Form("");
                QQQ[h] = new TextArea(2, 30);
                farr[h].addComponent(QQQ[h]);
                SingleQuestion(h);
                farr[h].addCommand(anse);


                farr[h].setCommandListener(this);
            }
            System.out.println(" After The big for loop");

            farr[0].show();
            Timer timer = new Timer();
            MyTimerTask timerTaskOne = new MyTimerTask();
            timer.schedule(timerTaskOne, 0, 1000);
            farr[0].setTitle(String.valueOf(Counter));
            System.out.println(" After The big for loop");
        } catch (Exception n) {
            n.printStackTrace();
        }
    }

    public void SingleQuestion(int Fno) {
        try {
            // this is the title of question
            int No = First.conn.in.readInt();

            String Ques = First.conn.in.readUTF();


            Container co = new Container();
            bg[Fno] = new ButtonGroup();
            GridLayout testLayout = new GridLayout(No, 1);

            co.setLayout(testLayout);
            mealPrefs = new RadioButton[No];
            farr[Fno].setLayout(testLayout);
            farr[Fno].setHeight(2);
            farr[Fno].setWidth(2);
            for (int y = 0; y < No; y++) {
                String ss = First.conn.in.readUTF();
                System.out.println(" recievied " + ss);
                mealPrefs[y] = new RadioButton(ss);

                mealPrefs[y].setTickerEnabled(false);
//                mealPrefs[y].setSize(new Dimension(42, 42));

                bg[Fno].add(mealPrefs[y]);
//                mealPrefs[y].repaint();
                co.addComponent(mealPrefs[y]);
                System.out.println(" ******* After QQQ " + Fno);

               

//                farr[Fno].addComponent(mealPrefs[y]);

            }

            farr[Fno].addComponent(co);

            QQQ[Fno].setText(Ques);
            QQQ[Fno].setEditable(false);

            farr[Fno].setCommandListener(this);
        } catch (IOException nm) {
            nm.printStackTrace();
        } catch (Exception m) {
            m.printStackTrace();
        }
    }

    //returns a list created from an image array and a text array
    private List getList(Image[] images, String[] texts) {
        int length = images.length;


        //the list will have 24 elements
        for (int i = 0; i < length; i++) {
            //repetitively use 6 images and strings
            int index = i % length;

            //Content1 is the object used as list element
            //create and load 24 Content1 objects
            Content1s[i] = new Content1(images[index], texts[index]);
        }

        return new List(Content1s);
    }

    //returns a list created from an image array and a text array
    private List getList_Subj(Image[] images, String[] texts) {
        int length = images.length;


        //the list will have 24 elements
        for (int i = 0; i < length; i++) {
            //repetitively use 6 images and strings
            int index = i % length;

            //Content1 is the object used as list element
            //create and load 24 Content1 objects
            Content1_Subj[i] = new Content1(images[index], texts[index]);
        }

        return new List(Content1_Subj);
    }

    //returns a list created from an image array and a text array
    private List getList_(Image[] images, String[] texts, Content1 con[]) {
        int length = texts.length;


        //the list will have 24 elements
        for (int i = 0; i < length; i++) {
            //repetitively use 6 images and strings
            int index = i % length;

            //Content1 is the object used as list element
            //create and load 24 Content1 objects
            con[i] = new Content1(images[index], texts[index]);
        }

        return new List(con);
    }

    public void ReciveQuizes() {  // retrive all quizez avialble in that subject
        try {

            Content1[] qui;
            int num = First.conn.in.readInt();

            if (num == 0) {

                notav = new Form("not Avialable Quize  ");

                Label b = new Label("quize not avialble  ");


                quizeNotAv = new Command("back");
                notav.addCommand(quizeNotAv);
                notav.setCommandListener(this);
                notav.addComponent(b);
                notav.show();
                System.out.println(" no avialable quize in that subject ");

            } else {
                quiz = new String[num];
                qui = new Content1[num];
                System.out.println(" number recieved to quizes " + num);
                for (int c = 0; c < num; c++) {
                    quiz[c] = First.conn.in.readUTF();
                    System.out.println(" recieved qqquuuquuuq " + quiz[c]);

                }

                li = getList_(images, quiz, qui);
                Quizes = new Form("available quize   ");
                //get an AlphaListRenderer instance and install
                AlphaListRenderer renderer = new AlphaListRenderer();
                li.setListCellRenderer(renderer);
                //get width of the form
                int width = Quizes.getWidth();
                //hint for alphaList size
                li.setPreferredSize(new Dimension(width - 2, li.getPreferredH()));
                Quizes.addComponent(li);
                Command com = new Command("Quize");
                Quizes.addCommand(com);

                Quizes.setCommandListener(this);
                Back_subj = new Command("prevois");
                Quizes.addCommand(Back_subj);
                Quizes.show();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    private void End_Quize_Send_Answer() {
        try {
            First.conn.out.writeInt(Answer.length);

            for (int g = 0; g < Answer.length; g++) {

                First.conn.out.writeInt(Answer[g]);
                First.conn.out.flush();
            }

            int final_result = First.conn.in.readInt();
            Form f = new Form("Exam Ended ");
            Label l = new Label("Your Final  Result is:");
            Label j = new Label("" + final_result);

            f.addComponent(l);
            f.addComponent(j);
            FinalC = new Command("main");
            f.addCommand(FinalC);
            f.setCommandListener(this);
            f.show();
        } catch (IOException ex) {
            ex.printStackTrace();
        }



    }

    private void ASK_QUES() {
        String material[] = null;
        try {
            int num = First.conn.in.readInt();
            System.out.println("Read number of material is: " + num);
            material = new String[num];
            for (int x = 0; x < num; x++) {
                material[x] = First.conn.in.readUTF();
                System.out.println("Name read is: " + material[x]);
            }






        } catch (IOException ex) {
            ex.printStackTrace();
        }
        //=========================
        final Form listForm = new Form("Available Material");
//
////create a font
        Font font = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_LARGE);

//set text color for title
//listForm.getTitleStyle().setFgColor(0x99cc00);
//listForm.setBgImage(img3);
//set font style for title
        listForm.getTitleStyle().setFont(font);

//set background color for the title bar
        listForm.getTitleStyle().setBgColor(0x555555);

//create a new style object
        Style menuStyle = new Style();

//set the background color -- the same as for title bar
        menuStyle.setBgColor(0x555555);

//set the text color for soft button
        menuStyle.setFgColor(0x99cc00);

//set font style for soft button
        menuStyle.setFont(font);

//now install the style for soft button
        listForm.setSoftButtonStyle(menuStyle);

//set a background color for the form
        listForm.getStyle().setBgColor(0x656974);

//create and add 'Exit' command to the form
//the command id is 0
        Command sel = new Command("Choose");
        listForm.addCommand(new Command("Back"));
        listForm.addCommand(sel);
//this MIDlet is the listener for the form's command

// Initialize a default list model with “item”
//String material[]={"ahemd ","ali"};
        DefaultListModel simpleListModel = new DefaultListModel(material);

// Create a List with “simpleListModel”
        final List simpleList = new List(simpleListModel);
///***************************************/
        listForm.setCommandListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                //showDialog();
                Command c = ae.getCommand();
                if (c.getCommandName().equals("Choose")) {
                    //Send PDF
                    if (SELCTION_List.x == 0) {
                        System.out.println(" Selected Index is " + simpleList.getSelectedIndex());
                        int x = simpleList.getSelectedIndex();
                        String cc = String.valueOf(x);

                        // showDialog("You select:"+cc);
                    } //Ask Question
                    else if (SELCTION_List.x == 1) {
                        System.out.println(" Selected Index is " + simpleList.getSelectedIndex());
                        /************************************/
                        String Mate = (String) simpleList.getSelectedItem();
                        System.out.println("" + Mate);
                        Form Ask = new Form("Enter Question");

                        //no scrollbar for the form
                        Ask.setScrollable(false);

                        //create a font
                        Font font = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_LARGE);

                        //set text color for title
                        Ask.getTitleStyle().setFgColor(0x99cc00);

                        //set font style for title
                        Ask.getTitleStyle().setFont(font);

                        //set background color for the title bar
                        Ask.getTitleStyle().setBgColor(0x555555);

                        //create a new style object
                        Style menuStyle = new Style();

                        //set the background color -- the same as for title bar
                        menuStyle.setBgColor(0x555555);

                        //set the text color for soft button
                        menuStyle.setFgColor(0x99cc00);

                        //set font style for soft button
                        menuStyle.setFont(font);

                        //now install the style for soft button
                        Ask.setSoftButtonStyle(menuStyle);

                        //set a background color for the form
                        Ask.getStyle().setBgColor(0x656974);

                        //create and add 'Exit' command to the form
                        //the command id is 0
                        Ask.addCommand(new Command("Send", 1));

                        //this MIDlet is the listener for the form's command
                        Ask.setCommandListener(this);

                        //style for text areas
                        Style txtStyle = new Style();
                        txtStyle.setFgColor(0xe8dd21);
                        txtStyle.setFgSelectionColor(0xe8dd21);
                        txtStyle.setBgColor(0xff0000);
                        txtStyle.setBgSelectionColor(0xff0000);
                        txtStyle.setBgTransparency(80);
                        txtStyle.setFont(Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_PLAIN, Font.SIZE_LARGE));
                        txtStyle.setBorder(Border.createRoundBorder(10, 7, 0xe8dd21));
                        UIManager.getInstance().setComponentStyle("TextArea", txtStyle);


                        //new TextArea
                        final TextArea Text = new TextArea(5, 10, TextArea.ANY);
                        Ask.addComponent(Text);

                        Text.addActionListener(new ActionListener() {

                            public void actionPerformed(ActionEvent ae) {
                                String Ques = Text.getText();
                                String Materiall = (String) simpleList.getSelectedItem();
                                try {
                                    First.conn.out.writeUTF(Ques);
                                    First.conn.out.flush();
                                    First.conn.out.writeUTF(Materiall);
                                    First.conn.out.flush();
                                } catch (IOException ex) {
                                    ex.printStackTrace();
                                }
                                listForm.show();

                            }
                        });

                        //show the form
                        Ask.show();
                        /*********************************/
                    }
                } else if (c.getCommandName().equals("Back")) {

                    try {

                        selectionForm.show();
                        First.conn.out.writeUTF("back");

                    } catch (Exception mm) {
                        mm.printStackTrace();
                    }
                }





            }
        });
        /*************************************/
//create a ListCellRenderer and install
        DefaultListCellRenderer dlcr = new DefaultListCellRenderer();
        simpleList.setListCellRenderer(dlcr);

//set prototype to ensure adequate width
        simpleList.setRenderingPrototype("WWWWWWWWWWWWWWWWWWWWWWWWWWW");

//set transparency for list
        simpleList.getStyle().setBgTransparency(64);
//create and set style for the default list cell renderer
//comment out the following 6 statements if styling is already done for all Labels
        Style lStyle = new Style();
        lStyle.setFgColor(0);
        lStyle.setFgSelectionColor(0xffffff);
        lStyle.setBgSelectionColor(0x0000ff);
        lStyle.setFont(Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_PLAIN, Font.SIZE_LARGE));
        dlcr.setStyle(lStyle);

//add list to form
        listForm.addComponent(simpleList);
        listForm.show();

        /****************************************/
//  List
    }

    class EndQuize extends TimerTask {                                 //=======  //// ====================

        public void ExameEnded() {
            endQ = new Form("");
            TextArea text = new TextArea(20, 30);
            endQ.addComponent(text);
            Command sen = new Command("Send Confirmation ");
            endQ.addCommand(sen);
            endQ.show();


        }

        public void run() {
            ExameEnded();
            System.out.println(" this is timer Task to end exmaes ");
        }
    }

    private class MyTimerTask extends TimerTask {

        public void run() {
            Counter++;
            farr[cuurent_form].setTitle(String.valueOf(Counter) + "/75 second ");
        }
    }

    public void ShowForm() {
        Display.init(this);

        /// first Form for selecting subject that he will take quize
        Style scrollStyle = new Style();
        scrollStyle.setFgColor(0x5555ff);
        scrollStyle.setBgColor(0xdddd99);
        UIManager.getInstance().setComponentStyle("Scroll",
                scrollStyle);
//create and set a style for scrollthumb
        Style scrollThumbStyle = new Style();
        scrollThumbStyle.setMargin(Component.LEFT, 1);
        UIManager.getInstance().setComponentStyle("ScrollThumb",
                scrollThumbStyle);
//create a new form
        demoForm = new Form("select subject ");
//no scrollbar for the form
        demoForm.setScrollable(false);
//get width of the form
        int width = demoForm.getWidth();


        subjects = getList_Subj(images, names);
        AlphaListRenderer renderer = new AlphaListRenderer();
        subjects.setListCellRenderer(renderer);
//hint for alphaList size
        subjects.setPreferredSize(new Dimension(width - 2, subjects.getPreferredH()));

        demoForm.addComponent(subjects);

        ok = new Command("OK");
        bak = new Command("back");

        demoForm.addCommand(ok);

        demoForm.addCommand(bak);
        demoForm.setCommandListener(this);
        demoForm.show();
    }
}

class Content1 {

    private Image letter;
    private String text;

    public Content1(Image i, String s) {
        letter = i;
        text = s;
    }

    public Image getIcon() {
        return letter;
    }

    public String getText() {
        return text;
    }
}

class AlphaListRenderer extends Label implements ListCellRenderer {
    //create new AlphaListRenderer

    public AlphaListRenderer() {
        super();
    }

    public Component getListCellRendererComponent(List list, Object value, int index, boolean isSelected) {
        //cast the value object into a Content1
        Content1 entry = (Content1) value;

        //get the icon of the Content1 and set it for this label
        setIcon(entry.getIcon());

        //get the text of the Content1 and set it for this label
        setText(entry.getText());

        //set transparency
        getStyle().setBgTransparency((byte) 128);

        //set background and foreground colors
        //depending on whether the item is selected or not
        if (isSelected) {
            getStyle().setBgColor(0x0000ff);
            getStyle().setFgColor(0xffffff);
        } else {
            getStyle().setBgColor(0xff0000);
        }

        return this;
    }

    //initialize for drawing focus
    public Component getListFocusComponent(List list) {
        setText("");
        setIcon(null);
        getStyle().setBgColor(0x0000ff);
        getStyle().setBgTransparency(80);
        return this;
    }

    public void repaint() {
    }
    ////////////////////////////// my classes labib
}
