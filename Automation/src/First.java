import com.sun.lwuit.TextField;
import com.sun.lwuit.events.DataChangedListener;
import com.sun.lwuit.list.DefaultListCellRenderer;
import com.sun.lwuit.Command;
import com.sun.lwuit.Dialog;
import com.sun.lwuit.Display;
import com.sun.lwuit.Font;
import com.sun.lwuit.Form;
import com.sun.lwuit.Image;
import com.sun.lwuit.Label;
import com.sun.lwuit.List;
import com.sun.lwuit.animations.Transition;
import com.sun.lwuit.events.ActionEvent;
import com.sun.lwuit.events.ActionListener;
import com.sun.lwuit.plaf.Border;
import com.sun.lwuit.plaf.Style;
import com.sun.lwuit.plaf.UIManager;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.microedition.io.*;
import javax.microedition.io.StreamConnection;
import javax.microedition.midlet.*;
public class First extends MIDlet implements ActionListener
{
static Connection conn;
    StreamConnection connection1;
	//the text field
	TextField demoText;
//duration of transition in milliseconds
	private int duration = 750;

	//number of steps in transition
	private int numOfSteps = 10;
    
	//the form
static Form mainForm;
Image img3= null;
    //Form for error
    Form error;
    Command back,sel;
SELCTION_List list=new SELCTION_List();
	//commands for changing text field behaviour
	//Command REPLACE_CMD = new Command("Overwrite");
	Command go = new Command("Go");

	public void startApp()
	{
     sel=new Command("Back");
       		//initialize the LWUIT Display
		//and register this MIDlet
       		Display.init(this);

		//create a new form
		//Form mainForm = new Form("TextField Demo");
		mainForm = new Form("Automation Test");
        error = new Form("Access violation");
back=new Command("Back");
error.addCommand(new Command("Exit"));
error.addCommand(back);
error.setCommandListener(this);
		//create a font
		Font font = Font.createSystemFont(Font.FACE_PROPORTIONAL,Font.STYLE_BOLD,Font.SIZE_LARGE);

		//set text color for title
		mainForm.getTitleStyle().setFgColor(0x99cc00);

		//set font style for title
		mainForm.getTitleStyle().setFont(font);
		//error.getTitleStyle().setFont(font);
Image img = null;
Image img1 = null;

        try {
            img = Image.createImage("/accessdenied.jpg");
            img1 = Image.createImage("/Visual_Exam_Manager.jpg");
            img3 = Image.createImage("/ba.PNG");

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        error.setBgImage(img);
	mainForm.setBgImage(img1);
		//set background color for the title bar
		mainForm.getTitleStyle().setBgColor(0x555555);

		//create a new style object
		Style menuStyle = new Style();

		//set the background color -- the same as for title bar
		menuStyle.setBgColor(0x555555);

		//set the text color for soft button
		menuStyle.setFgColor(0x99cc00);

		//set font style for soft button
		menuStyle.setFont(font);

		//now install the style for soft button
		mainForm.setSoftButtonStyle(menuStyle);

		mainForm.getMenuStyle().setBgColor(0x555555);

		//set a background color for the form
		//mainForm.getStyle().setBgColor(0x656974);


		//create and add 'Exit', 'Resize'
		//and 'Overwrite' commands to the form
		mainForm.addCommand(new Command("Exit"));
		mainForm.addCommand(go);
		//this MIDlet is the listener for the form's commands
		mainForm.setCommandListener(this);

		//set background color for the menu
		mainForm.getMenuStyle().setBgColor(0x555555);

		//commands on menu form a list
		//so set a renderer for the list
		//this helps in setting a style for menu
		DefaultListCellRenderer dlcr = new DefaultListCellRenderer();
		mainForm.setMenuCellRenderer(dlcr);

		//set style for the menu
		Style mStyle = new Style();
		mStyle.setFgColor(0x99cc00);
		mStyle.setFgSelectionColor(0xffffff);
		mStyle.setBgSelectionColor(0x0000ff);
		mStyle.setFont(Font.createSystemFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
		dlcr.setStyle(mStyle);

		//set style for soft buttons
		Style sbStyle = new Style();
		sbStyle.setBgColor(0x555555);
		sbStyle.setFgColor(0x99cc00);
		sbStyle.setFont(Font.createSystemFont(Font.FACE_PROPORTIONAL,Font.STYLE_BOLD,Font.SIZE_MEDIUM));
		UIManager.getInstance().setComponentStyle("SoftButton", sbStyle);

		//set style for text fields
		Style txtStyle = new Style();
		txtStyle.setFgColor(0xff0000);
		txtStyle.setFgSelectionColor(0xe8dd21);
		txtStyle.setBgColor(0x0000ff);
		txtStyle.setBgSelectionColor(0xff0000);
		txtStyle.setBgTransparency(80);
		txtStyle.setFont(Font.createSystemFont(Font.FACE_PROPORTIONAL,Font.STYLE_BOLD,Font.SIZE_MEDIUM));
		txtStyle.setBorder(Border.createRoundBorder(10, 7, 0xe8dd21));
		UIManager.getInstance().setComponentStyle("TextField", txtStyle);

		//set the name to be given to 'T9' command
		TextField.setT9Text("Edit");

		//new text field
		demoText = new TextField(15);
             Label x=new Label("Enter ID");
             x.setStyle(sbStyle);
                Style sbStyle1 = new Style();
		sbStyle1.setBgColor(0xff0000);
		sbStyle1.setFgColor(0x0000ff);
                demoText.setStyle(txtStyle);

		//add listener to track data change
		demoText.addDataChangeListener(new DataChangedListener()
			{
				public void dataChanged(int type, int index)
				{
					//check if data is added
					if(type == 1)
					{
						System.out.println("New data has been added at position " + index);
					}
				}
			});

		//don't remove form commands
		demoText.setReplaceMenu(false);

                mainForm.addComponent(x);
		//add text field to form
		mainForm.addComponent(demoText);

        Transition out = new BlindsTransition(duration, numOfSteps, Display.getInstance().getDisplayWidth()/10);
            mainForm.setTransitionOutAnimator(out);
            error.setTransitionOutAnimator(out);
            mainForm.show();
   	}

   	public void pauseApp()
	{
   	}

   	public void destroyApp(boolean unconditional)
	{
   	}

	public void actionPerformed(ActionEvent ae)
	{
		Command cmd = ae.getCommand();

        	if(cmd.getCommandName().equals("Exit"))
		{
      			notifyDestroyed();
		}
            else if(cmd.getCommandName().equals("Go")){
           /*******Go to selection List******/
               //launsh midlet
                
                 String id=demoText.getText();


            conn=new Connection(this,id);

            conn.start();

            }
            else if(cmd.getCommandName().equals("Back")){
            mainForm.show();
            }
	
	}

    /********************Connection***********************/
 class Connection extends Thread{

byte []imgData=null;
     String Id;
public   String name=new String();
  public  String Describtion=new String();

  DataInputStream in;

  DataOutputStream out;
 //InputStream is;
 First midlet;
List simpleList;
        public Connection(First vot,String ID) {
            this.midlet= vot;
            this.Id=ID;




        }
 public void run() {
            try {
                setConnection();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
  }
 public void setConnection() throws IOException
 {
     try {
     connection1 = (StreamConnection) Connector.open("socket://127.0.0.1:7890", Connector.READ_WRITE);
     in= connection1.openDataInputStream();
     out=connection1.openDataOutputStream();
 System.out.println("/**********************************************/");
 System.out.println("/*****Intialization Connection*****************/");
 System.out.println("/**********************************************/");

     out.writeInt(1);
     out.flush();
         System.out.println("----------> Send to Server 1 ");
     int rr=in.readInt();
     out.flush();

     System.out.println("<--------- Recive From server:   "+rr);
     out.writeUTF(Id);
     out.flush();
         System.out.println("ID send to Server-------------> "+Id);
     int response = in.readInt();
     out.flush();
         System.out.println("<------------Recive Response from server:   "+response);
         response=300;                                      ///$$$%^&&**&^%$#@)(*$# should be removeddv
      check(response);


     }
           catch(IOException we)
           {
              showDialog();


   we.printStackTrace();
           }
 }
public void check(int respon) {
    System.out.println(" in method check responce");

                if(respon==300){
                    System.out.println(" trying to start list");
                  list.startApp();
                  list.selectionForm.show();
                  System.out.println("Your Login legal");
                }
                else if(respon==404){
                    error.show();
                    System.out.println("Your Login illegal");
                }


}
private void showDialog()
	{
		//create the dialog
		Dialog d = new Dialog("Error");

		//give it a border
		d.getContentPane().getStyle().setBorder(Border.createBevelRaised());

		//font for the label
		Font f = Font.createSystemFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM);

		//create label for heading
		Label hLabel = new Label("Network Error ");

		hLabel.getStyle().setFont(Font.createSystemFont(Font.FACE_SYSTEM,Font.STYLE_BOLD,Font.SIZE_LARGE));
		hLabel.getStyle().setBgTransparency(0);
		hLabel.getStyle().setFgColor(0);
		hLabel.getStyle().setMargin(Label.BOTTOM, 15);

		//add the heading to dialog
		d.addComponent(hLabel);

		//get selected item
//		Content1 selected = (Content1)simpleList.getSelectedItem();

		//create label for showing selected item
		

		//sLabel.setIcon(selected.getIcon());

		

		//set bg color for dialog
		d.getContentPane().getStyle().setBgColor(0xff8040);

		//set style attributes for the dialog titlebar
		d.getTitleStyle().setBgColor(0xa43500);
		d.getTitleStyle().setFgColor(0xffffff);
		d.getTitleStyle().setFont(Font.createSystemFont(Font.FACE_SYSTEM,Font.STYLE_BOLD,Font.SIZE_MEDIUM));
		d.getTitleStyle().setBorder(Border.createBevelRaised());

		//create and set style for dialog menubar
		Style s = new Style();
		s.setBgColor(0xa43500);
		s.setFgColor(0xffffff);
		s.setFont(Font.createSystemFont(Font.FACE_SYSTEM,Font.STYLE_BOLD,Font.SIZE_MEDIUM));
		s.setBorder(Border.createBevelRaised());
		d.setSoftButtonStyle(s);

		//add two commands to dialog
		d.addCommand(new Command("OK"));
		//d.addCommand(new Command("Back"));
                 d.setCommandListener(new ActionListener()
{
     public void actionPerformed(ActionEvent ae)
{
					//showDialog();
                    Command c = ae.getCommand();
                   if(c.getCommandName().equals("OK")){
                        try {
                    
                            mainForm.show();
                   
                          
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                   }
				}
			});
		//show the dialog
		d.showDialog();
	}



}
        	
}