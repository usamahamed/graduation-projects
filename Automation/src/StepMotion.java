public class StepMotion
{
    	private int destinationValue;
    	private int duration;
    	private int steps;
	private int stepNumber;
	private int stepSize;
    	private long startTime;
	private int interval;
	private int offset;
    
    	public StepMotion(int sourceValue, int destinationValue, int duration, int steps)
    	{
        	this.destinationValue = destinationValue;
        	this.duration = duration;
		this.steps = steps;

		//the size of a step
		stepSize = (destinationValue - sourceValue)/steps;

		//the time interval between two successive steps
		interval = duration/steps;
    	}

	//save the time when the motion starts
    	public void start()
	{
        	startTime = System.currentTimeMillis();
    	}

	//return true if all steps have been taken care of
	//and destination reached
    	public boolean isFinished()
	{
		return stepNumber > steps && offset == destinationValue;
    	}

	//return the width
    	public int getStep()
    	{
		//check if interval for next step is over
		//if so increment stepNumber and return (stepSize*stepNumber)
		//if (stepSize*stepNumber>destinationValue) then return destinationValue
		//if interval not over return -1

		if(System.currentTimeMillis() >= startTime + interval*(stepNumber+1))
		{
			stepNumber++;
			offset = stepNumber*stepSize;
			if(offset > destinationValue)
			{
				offset = destinationValue;
			}

			return offset;
		}
		else
		{
			return -1;
		}
    	}
}
