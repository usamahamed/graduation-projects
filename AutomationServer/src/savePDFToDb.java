import java.sql.*;
import java.io.*;

public class savePDFToDb {
    public static  DB db = new DB();
    public savePDFToDb() {

        Connection conn=db.dbConnect(
                "jdbc:mysql://localhost/autotest","root","root");

        //db.insertPDF(conn,12,"math.pdf","math.pdf");
      //  db.getPDFData(conn,"math.pdf");
    }
 public static void main(String [] args) {

        Connection conn=db.dbConnect(
                "jdbc:mysql://localhost/autotest","root","root");

        db.insertPDF(conn,12,"ch3.pdf","ch3.pdf");
      //  db.getPDFData(conn,"math.pdf");
    }

}

class DB {
    public DB() {}

    public Connection dbConnect(String db_connect_string,
            String db_userid, String db_password) {
        try {
             Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(
                    db_connect_string, db_userid, db_password);

            System.out.println("connected");
            return conn;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void insertPDF(Connection conn,int id,String Mat_Name,String filename) {
        int len;
        String query;
        PreparedStatement pstmt;

        try {
            File file = new File(filename);
            FileInputStream fis = new FileInputStream(file);
            len = (int)file.length();
            query = ("insert into material VALUES(?,?,?)");
            pstmt = conn.prepareStatement(query);
           pstmt.setInt(1, id);
            pstmt.setString(2,Mat_Name);
           // 

            //method to insert a stream of bytes
            pstmt.setBinaryStream(3, fis, len);
            pstmt.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getPDFData(Connection conn,String Name) {

        byte[] fileBytes = null;
        String query;
        try {
            query =
              "select * from material where File_name='"+Name+"'";
            Statement state = conn.createStatement();
            ResultSet rs = state.executeQuery(query);
            if (rs.next()) {
                fileBytes = rs.getBytes(3);
                OutputStream targetFile=
                        new FileOutputStream(new File("Name"));
                targetFile.write(fileBytes);
                targetFile.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return Name;
    }
};