/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Subject_Result.java
 *
 * Created on Jul 5, 2010, 2:01:14 PM
 */


import javax.swing.JFrame;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Mahmoud
 */
public class Subject_Result extends javax.swing.JPanel {
    Connection conn;
    Statement stat;
    Statement stat1;
    Statement stat2;
    Statement stat3;
    String Query;
    ResultSet rs;
    ResultSet rs1;
    ResultSet rs2;
    ResultSet rs3;
    int i = 0;
    /** Creates new form Subject_Result */
    public Subject_Result(int DR_ID) {
        initComponents();
        try{
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Driver loaded");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/autotest", "root", "root");
            System.out.println("Database connected");
            stat = conn.createStatement();
            stat1 = conn.createStatement();
            stat2 = conn.createStatement();
            stat3 = conn.createStatement();
            Query = "select Subject_name from subject where DR_ID = " + DR_ID;
            rs3 = stat3.executeQuery(Query);
            while(rs3.next())
                jComboBox1.addItem(rs3.getString(1));
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox1 = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setBackground(new java.awt.Color(255, 255, 255));
        setPreferredSize(new java.awt.Dimension(589, 462));

        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        jLabel1.setText("Choose Subject");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Student_name", "Quize_ID", "Answer"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 569, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(172, 172, 172)
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(89, 89, 89)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(48, 48, 48)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(166, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        try {
            if(jComboBox1.getSelectedItem() != null){
                for(int j = 0;j < jTable1.getRowCount();j++)
                    for(int k = 0; k < jTable1.getColumnCount();k++)
                        jTable1.setValueAt("", j, k);
                Query = "select Subject_ID from subject where Subject_name = '" + jComboBox1.getSelectedItem() + "'" ;
                rs = stat.executeQuery(Query);
                while(rs.next()){
                    Query = "select Student_ID,Quize_ID,Answer from result where Subject_ID = " + rs.getInt(1);
                    rs1 = stat1.executeQuery(Query);
                    while(rs1.next()){
                        Query = "select Student_name from student where Student_ID = " + rs1.getInt(1);
                        rs2 = stat2.executeQuery(Query);
                        rs2.next();
                        jTable1.setValueAt(rs1.getString(3), i, 2);
                        jTable1.setValueAt(rs2.getString(1), i, 0);
                        jTable1.setValueAt(rs1.getString(2), i, 1);
                        i++;
                    }
                }
                i = 0;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Subject_Result.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jComboBox1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables


}