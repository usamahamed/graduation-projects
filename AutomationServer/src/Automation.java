import java.awt.*;
import javax.swing.*;

public class Automation extends JWindow {

    private int duration;

    public Automation(int d) {
        duration = d;
    }

     public void showSplash() {

        JPanel content = (JPanel)getContentPane();
        content.setBackground(Color.white);

     
        int width = 600;
        int height =350;
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (screen.width-width)/2;
        int y = (screen.height-height)/2;
        setBounds(x,y,width,height);

    
        JLabel label = new JLabel(new ImageIcon("1.png"));
        JLabel copyrt = new JLabel
                ("Welcome to Lion automation test Program", JLabel.CENTER);
        copyrt.setFont(new Font("Sans-Serif", Font.BOLD, 12));
        content.add(label, BorderLayout.CENTER);
        content.add(copyrt, BorderLayout.SOUTH);
        Color oraRed = new Color(156, 20, 20,  255);
        content.setBorder(BorderFactory.createLineBorder(oraRed, 10));


        setVisible(true);

      
        try { Thread.sleep(duration); } catch (Exception e) {}

        setVisible(false);

    }

    public void showSplashAndExit() {

        showSplash();
       

    }

    public static void main(String[] args) {

      
        Automation splash = new Automation(5000);

   
       
        splash.showSplashAndExit();
        login xx=new login();
        xx.setVisible(true);
        xx.setSize(1000,1000);
         System.out.println("Finish");
    }
}