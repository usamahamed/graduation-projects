
import com.mysql.jdbc.Connection;
//import com.mysql.jdbc.ResultSet;
import com.mysql.jdbc.Statement;
import java.sql.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Student_Operation.java
 *
 * Created on 29/06/2010, 09:27:36 م
 */

/**
 *
 * @author alaa
 */
public class Student_Operation extends javax.swing.JPanel {
        public static  int oldid;
        public static  int sflag=0;

    /** Creates new form Student_Operation */
    public Student_Operation() {
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jButton2 = new javax.swing.JButton();
        jTextField2 = new javax.swing.JTextField();
        jComboBox1 = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jRadioButton2 = new javax.swing.JRadioButton();
        jButton3 = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jRadioButton1 = new javax.swing.JRadioButton();
        jComboBox2 = new javax.swing.JComboBox();
        jTextField4 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        jButton2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton2.setForeground(new java.awt.Color(0, 0, 255));
        jButton2.setText("Update");
        jButton2.setEnabled(false);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jTextField2.setEditable(false);

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "CS", "IT", "IS", "OR", "Other" }));
        jComboBox1.setEnabled(false);
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton1.setForeground(new java.awt.Color(0, 0, 255));
        jButton1.setText("Search");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        buttonGroup1.add(jRadioButton2);
        jRadioButton2.setText("Female");
        jRadioButton2.setEnabled(false);

        jButton3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton3.setForeground(new java.awt.Color(0, 0, 255));
        jButton3.setText("Delete");
        jButton3.setEnabled(false);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jLabel8.setText("Other : ");

        buttonGroup1.add(jRadioButton1);
        jRadioButton1.setSelected(true);
        jRadioButton1.setText("Male");
        jRadioButton1.setEnabled(false);

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "First", "Second", "Third", "Fourth" }));
        jComboBox2.setEnabled(false);

        jTextField4.setEditable(false);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14));
        jLabel7.setForeground(new java.awt.Color(0, 0, 255));
        jLabel7.setText("Gender : ");

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14));
        jLabel1.setForeground(new java.awt.Color(0, 0, 255));
        jLabel1.setText("Year : ");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14));
        jLabel3.setForeground(new java.awt.Color(0, 0, 255));
        jLabel3.setText("Student_name : ");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 0, 255));
        jLabel2.setText("Student_ID : ");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 0, 255));
        jLabel5.setText("Student Operations");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14));
        jLabel4.setForeground(new java.awt.Color(0, 0, 255));
        jLabel4.setText("Department : ");

        jTextField3.setEditable(false);

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 0, 255));
        jLabel6.setText("Student_Password : ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(287, 287, 287)
                        .addComponent(jLabel5))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(57, 57, 57)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel1)
                            .addComponent(jLabel7)
                            .addComponent(jLabel2))
                        .addGap(23, 23, 23)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTextField1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jRadioButton1)
                                .addGap(29, 29, 29)
                                .addComponent(jRadioButton2))
                            .addComponent(jComboBox2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(60, 60, 60)
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(103, 103, 103)
                        .addComponent(jButton1)
                        .addGap(43, 43, 43)
                        .addComponent(jButton3)
                        .addGap(49, 49, 49)
                        .addComponent(jButton2)))
                .addContainerGap(70, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel5)
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel4)
                        .addGap(21, 21, 21)
                        .addComponent(jLabel1)
                        .addGap(21, 21, 21)
                        .addComponent(jLabel7))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8)
                            .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jRadioButton1)
                            .addComponent(jRadioButton2))))
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton3)
                    .addComponent(jButton2))
                .addContainerGap(46, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        
        try {
            int id=Integer.parseInt(jTextField1.getText());
        String name =jTextField2.getText();
        String password =jTextField3.getText();


        int departmentindex=jComboBox1.getSelectedIndex();


        String department="";
        if(departmentindex==0) {
            department="CS";
        } else if(departmentindex==1) {
            department="IT";
        } else if(departmentindex==2) {
            department="IS";
        } else if(departmentindex==3) {
            department="OR";
        } else if(departmentindex==4) {
            department=jTextField4.getText();
        }



        int yearindex=jComboBox2.getSelectedIndex();
        String year="";
        if(yearindex==0) {
            year="First";
        } else if(yearindex==1) {
            year="Second";
        } else if(yearindex==2) {
            year="Third";
        } else if(yearindex==3) {
            year="Fourth";
        }

        boolean radio1=jRadioButton1.isSelected();
        String gender="";
        if(radio1) {
            gender="Male";
        } else {
            gender="Female";
        }
            update(oldid, id, name, department, year, gender,password);
            // TODO add your handling code here:
        } catch (Exception ex) {
JOptionPane.showMessageDialog(null,"Student ID  must Integer","error",JOptionPane.ERROR_MESSAGE);

        }
        jTextField1.setText("");
        jTextField3.setText("");
        jTextField2.setText("");
        jTextField4.setText("");
        jComboBox1.setSelectedIndex(0);
        jComboBox2.setSelectedIndex(0);
        jRadioButton1.setSelected(true);
        jTextField2.setEditable(false);
        jTextField3.setEditable(false);
        jTextField4.setEditable(false);
        jButton2.setEnabled(false);
        jButton3.setEnabled(false);
        jButton1.setEnabled(true);
        jRadioButton1.setEnabled(false);
        jRadioButton2.setEnabled(false);
        jComboBox1.setEnabled(false);
        jComboBox2.setEnabled(false);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        int a=jComboBox1.getSelectedIndex();
        if(a==4) {
            jTextField4.setEditable(true);
        }
}//GEN-LAST:event_jComboBox1ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       
        try {
             int id=Integer.parseInt(jTextField1.getText());
            search(id);
            oldid=id;
            // TODO add your handling code here:
        } catch (Exception ex) {
         JOptionPane.showMessageDialog(null,"Student ID must Integer","error",JOptionPane.ERROR_MESSAGE);

        }
if(sflag==1)
{
        jTextField2.setEditable(true);
        jTextField3.setEditable(true);
        jComboBox1.setEnabled(true);
        jComboBox2.setEnabled(true);
        jRadioButton1.setEnabled(true);
        jRadioButton2.setEnabled(true);
        jButton2.setEnabled(true);
        jButton3.setEnabled(true);
        jButton1.setEnabled(true);
}
else
        {
                 jTextField1.setText("");
        jTextField2.setText("");
        jTextField3.setText("");

        jTextField2.setEditable(false);
        jTextField3.setEditable(false);

        jButton2.setEnabled(false);
        jButton3.setEnabled(false);
        jButton1.setEnabled(true);
        jTextField1.setText("");
        jTextField2.setText("");
        jTextField4.setText("");
        jComboBox1.setSelectedIndex(0);
        jComboBox2.setSelectedIndex(0);
        jRadioButton1.setSelected(true);
        jTextField2.setEditable(false);
        jTextField4.setEditable(false);
        jButton2.setEnabled(false);
        jButton3.setEnabled(false);
        jButton1.setEnabled(true);
        jRadioButton1.setEnabled(false);
        jRadioButton2.setEnabled(false);
        jComboBox1.setEnabled(false);
        jComboBox2.setEnabled(false);
        }

    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        
        try {
            int id=Integer.parseInt(jTextField1.getText());
            delete(id);
            // TODO add your handling code here:
        } catch (Exception ex) {
 JOptionPane.showMessageDialog(null,"Student ID must Integer","error",JOptionPane.ERROR_MESSAGE);

        }
        jTextField1.setText("");
        jTextField2.setText("");
        jTextField3.setText("");

        jTextField2.setEditable(false);
        jTextField3.setEditable(false);

        jButton2.setEnabled(false);
        jButton3.setEnabled(false);
        jButton1.setEnabled(true);
        jTextField1.setText("");
        jTextField2.setText("");
        jTextField4.setText("");
        jComboBox1.setSelectedIndex(0);
        jComboBox2.setSelectedIndex(0);
        jRadioButton1.setSelected(true);
        jTextField2.setEditable(false);
        jTextField4.setEditable(false);
        jButton2.setEnabled(false);
        jButton3.setEnabled(false);
        jButton1.setEnabled(true);
        jRadioButton1.setEnabled(false);
        jRadioButton2.setEnabled(false);
        jComboBox1.setEnabled(false);
        jComboBox2.setEnabled(false);


    }//GEN-LAST:event_jButton3ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JComboBox jComboBox2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    // End of variables declaration//GEN-END:variables
 public void search(int s_ID) throws SQLException
    {


sflag=0;
               Connection connection = null;

              try {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Driver loaded");
            connection = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/autotest", "root", "root");
            System.out.println("Database connected");
        } catch(SQLException sql)
{
    sql.printStackTrace();
}
catch(Exception ex)
{
    ex.printStackTrace();
}




         String str;

     Statement stat;
    stat=(Statement) connection.createStatement();
         str="SELECT * FROM student where  Student_ID= '"+s_ID+"'";

			ResultSet rs = (ResultSet) stat.executeQuery(str);

String s_id = null,s_name = null,s_gender = null,s_dept = null,s_year=null,password=null;
			while(rs.next())
			{
                            sflag=1;
                        System.out.println("ohloop");
			s_id=rs.getString(1);
                        s_name=rs.getString(2);
                        s_gender=rs.getString(3);
                        s_dept=rs.getString(4);
                        s_year=rs.getString(5);
                        password=rs.getString(7);

			}
if(sflag==1)
{
             jTextField1.setText(s_id);
             jTextField2.setText(s_name);
             jTextField3.setText(password);

             if(s_dept.equals("CS"))
             {
                 jComboBox1.setSelectedIndex(0);
             }
             else if(s_dept.equals("IT"))
             {
               jComboBox1.setSelectedIndex(1);
             }

             else if(s_dept.equals("IS"))
             {
               jComboBox1.setSelectedIndex(2);
             }

             else  if(s_dept.equals("OR"))
             {
               jComboBox1.setSelectedIndex(3);
             }
             else
             {
                 jTextField4.setText(s_dept);
                 jComboBox1.setSelectedIndex(4);
             }



                 if(s_year.equals("First"))
             {
                 jComboBox2.setSelectedIndex(0);
             }
             else if(s_year.equals("Second"))
             {
               jComboBox2.setSelectedIndex(1);
             }

             else if(s_year.equals("Third"))
             {
               jComboBox2.setSelectedIndex(2);
             }

             else  if(s_year.equals("Fourth"))
             {
               jComboBox2.setSelectedIndex(3);
             }


              if(s_gender.equals("Male"))
             {
               jRadioButton1.setSelected(true);
             }
              else
              {
               jRadioButton2.setSelected(true);
              }
}
else
{
 JOptionPane.showMessageDialog(null,"Student Not Found","error",JOptionPane.ERROR_MESSAGE);
 
}



    }
    public void delete(int s_ID) throws SQLException
    {



              Connection connection = null;

              try {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Driver loaded");
            connection = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/autotest", "root", "root");
            System.out.println("Database connected");
        } catch(SQLException sql)
{
    sql.printStackTrace();
}
catch(Exception ex)
{
    ex.printStackTrace();
}


     String str3;
     Statement stat3;
    stat3=(Statement) connection.createStatement();
         str3="delete  FROM  result where Student_ID = '"+s_ID+"'";

			int rs3 =  stat3.executeUpdate(str3);


             String str;
     Statement stat;
    stat=(Statement) connection.createStatement();
         str="delete  FROM  student where Student_ID = '"+s_ID+"'";

			int rs =  stat.executeUpdate(str);
    }
    public void update(int old_s_id, int s_id,String name,String dept,String year,String gender,String password) throws SQLException
    {


        int neww=old_s_id;


              Connection connection = null;

              try {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Driver loaded");
            connection = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/autotest", "root", "root");
            System.out.println("Database connected");
        } catch(SQLException sql)
{
    sql.printStackTrace();
}
catch(Exception ex)
{
    ex.printStackTrace();
}


        String Query1;
         String Query2;
        Statement stat;
       stat = (Statement) connection.createStatement();

 Query1 = "update student set Student_ID ='"+s_id+"' where Student_ID = '" + old_s_id+ "'";
             stat.executeUpdate(Query1);





            Query2 = "update student set Student_name='"+name+"',Gender='"+gender+"',Department='"+dept+"',Year='"+year+"',Password = '"+password+"' where Student_ID = '" + s_id+ "'";
             stat.executeUpdate(Query2);




    }
}
