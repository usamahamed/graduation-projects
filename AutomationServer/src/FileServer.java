import java.net.*;
import java.io.*;

public class FileServer {
  public static void main (String [] args ) throws IOException {
    // create socket
    ServerSocket servsock = new ServerSocket(13267);
    while (true) {
      System.out.println("Waiting...");

      Socket sock = servsock.accept();
      System.out.println("Accepted connection : " + sock);

      // sendfile
      File myFile = new File ("source.pdf");
     // byte [] mybytearray  = new byte [(int)myFile.length()];
      InputStream fis = new FileInputStream(myFile);
     OutputStream os = sock.getOutputStream();
      System.out.println("Sending...");
      byte[] buff = new byte[sock.getSendBufferSize()];
     // os.write(mybytearray,0,mybytearray.length);
      int bytesRead = 0;
      System.out.println(myFile.length() +" bytes");

while((bytesRead = fis.read(buff))>0)
{
os.write(buff,0,bytesRead);
}
fis.close();
      os.flush();
      sock.close();
      }
    }
}
